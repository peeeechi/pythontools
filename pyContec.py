import sys,os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from caio import*
import time
import struct
import numpy as np
import pandas as pd
import threading
from logging import getLogger, basicConfig, FileHandler, StreamHandler
from logging import NOTSET, DEBUG,INFO, WARNING, ERROR, CRITICAL

##
# @breif comment
class PyContec(object):
    """
    ### Breif
    comment\n
    ### Args
    * _type_ deviceName :
    \n
    ### Return
    * _type_ value :
    """
    ##
    # @breif comment
    # @param self
    # @return
    def __init__(self, deviceName):
        """

        """
        # 初期値設定
        self.caio           = Caio()
        self.logger         = getLogger(__name__)
        self.StartFlg       = False
        self.StopFlg        = False
        self.MaxDataCount   = 30000000
        self.SafeCount      = 10000
        self.DeviceName     = deviceName
        self.Id             = None
        self.Channels       = 1
        self.MemoryType     = 0
        self.MemorySize     = 750 * 1024
        self.Range          = C_InOut_Ranges.PM10
        self.SamplingClock  = float(500)


        return
    ##
    # @breif デバイスを初期化し、測定条件を設定します\n
    #        ※ 測定前は必ず実行してください
    # @param int Channels       :
    # @param int Range          :
    # @param int MemoryType     :
    # @param int SamplingClock  : ScanRate (600～107374182[μSec])
    def Init(self, Channels = None, Range = None, MemoryType = None, MemorySize = None, SamplingClock = None):
        """
        ### Breif
        デバイスを初期化し、測定条件を設定します\n
        __※ 測定前は必ず実行してください__
        ### Args
        * _int_ Channels        :
        * _int_ Range           :
        * _int_ MemoryType      :
        * _int_ MemorySize      :
        * _int_ SamplingClock   : ScanRate (600～107374182[μSec])
        \n
        """

        if Channels != None:
            self.Channels       = Channels
        if Range != None:
            self.Range          = Range
        if MemoryType != None:
            self.MemoryType     = MemoryType
        if MemorySize != None:
            self.MemorySize     = MemorySize
        if SamplingClock != None:
            self.SamplingClock  = float(SamplingClock)

        # 初期化、ID取得
        ret, _name, self.Id = self.caio.Init(bytes(self.DeviceName,'utf-8'), 0)
        self.DeviceName     = _name.decode(Caio.encode)
        if self.logger != None:
            self.logger.debug("Init: {0}, {1}, {2}".format(ret, self.DeviceName, self.Id))
        self._getErrorCode(ret)

        try:
            # デバイスのリセット
            ret = self.caio.ResetDevice(self.Id)
            if self.logger != None:
                self.logger.debug("ResetDevice: {0}".format(ret))
            self._getErrorCode(ret)

            # 使用可能チャンネル数の確認
            AiMaxChannels = 0
            ret, AiMaxChannels = self.caio.GetAiMaxChannels(self.Id, AiMaxChannels)
            if self.logger != None:
                self.logger.debug("GetAiMaxChannels: {0}, {1}".format(ret, AiMaxChannels))
            self._getErrorCode(ret)

            if AiMaxChannels == 0:
                if self.logger != None:
                    self.logger.error("アナログ入力の機能を持っていません")
                raise AssertionError("アナログ入力の機能を持っていません")

            #メモリーサイズの設定
            # ret = self.caio.SetAiMemorySize(self.Id, self.MemorySize)
            # if self.logger != None:
            #     self.logger.debug("SetAiMemorySize: {0}".format(ret))
            # self._getErrorCode(ret)

            # 測定レンジの設定
            ret = self.caio.SetAiRangeAll(self.Id, self.Range)
            if self.logger != None:
                self.logger.debug("SetAiRangeAll: {0}".format(ret))
            self._getErrorCode(ret)

            # チャネル数の設定
            ret = self.caio.SetAiChannels(self.Id, self.Channels)
            if self.logger != None:
                self.logger.debug("SetAiChannels: {0}".format(ret))
            self._getErrorCode(ret)

            #メモリタイプの設定
            ret = self.caio.SetAiMemoryType(self.Id , self.MemoryType)
            if self.logger != None:
                self.logger.debug("SetAiMemoryType: {0}".format(ret))
            self._getErrorCode(ret)

            #クロック
            #AioSetAiClockType
            ret = self.caio.SetAiClockType(self.Id, 0)
            if self.logger != None:
                self.logger.debug("SetAiClockType: {0}".format(ret))
            self._getErrorCode(ret)

            # スキャンレート設定
            ret = self.caio.SetAiSamplingClock(self.Id, self.SamplingClock)
            if self.logger != None:
                self.logger.debug("SetAiSamplingClock: {0}".format(ret))
            self._getErrorCode(ret)

            # サンプリング回数の設定
            ret = self.caio.SetAiStopTimes(self.Id, 1)
            if self.logger != None:
                self.logger.debug("SetAiStopTimes: {0}".format(ret))
            self._getErrorCode(ret)

            #開始条件
            ret = self.caio.SetAiStartTrigger(self.Id, 0)
            if self.logger != None:
                self.logger.debug("SetAiStartTrigger: {0}".format(ret))
            self._getErrorCode(ret)

            #終了条件
            ret = self.caio.SetAiStopTrigger(self.Id, 4)
            if self.logger != None:
                self.logger.debug("SetAiStopTrigger: {0}".format(ret))
            self._getErrorCode(ret)

            if self.logger != None:
                self.logger.debug("設定正常終了")
        except Exception as e:
            self.logger.error(e.msg)
            ret = self.caio.Exit(self.Id)
            if self.logger != None:
                self.logger.debug("Exit: {0}".format(ret))
            self._getErrorCode(ret)

            raise AttributeError()


    ##
    # @breif comment
    # @param
    # @return
    def Start(self):
        """
        ### Breif
        データ取得を開始します\n
        ### Args
        * _type_ ArgName :
        \n
        ### Return
        * _type_ value :
        """

        # バッファメモリをクリアし、サンプリング開始 ------------------------------------------------------------------

        self.StopFlg    = False
        self.StartFlg   = True

        try:
            # AioResetAiMemory
            ret = self.caio.ResetAiMemory(self.Id)
            if self.logger != None:
                self.logger.debug("ResetAiMemory: {0}".format(ret))
            self._getErrorCode(ret)

            # AioStartAi
            ret = self.caio.StartAi(self.Id)
            if self.logger != None:
                self.logger.debug("SetAiInputMethod: {0}".format(ret))
            self._getErrorCode(ret)

            # 終了フラグがTrueになるか、最大データ量をオーバーするまでサンプリング ------------------------------------------

            bufSamplingCount    = 0
            dataList            = []
            while True:

                # メモリ内格納サンプリング数の取得
                AiSamplingCount      =  0
                ret, AiSamplingCount =  self.caio.GetAiSamplingCount(self.Id, AiSamplingCount)
                if self.logger != None:
                    self.logger.debug("AiSamplingCount: {0}".format(AiSamplingCount))
                self._getErrorCode(ret)

                # 状態取得
                AiStatus      = 0
                ret, AiStatus = self.caio.GetAiStatus(self.Id, AiStatus)
                if self.logger != None:
                    self.logger.debug("GetAiStatus: {0}".format(hex(AiStatus)))
                self._getErrorCode(ret)

                if self.StopFlg == True:
                    break

                if AiSamplingCount + bufSamplingCount >= self.MaxDataCount:
                    self.logger.debug("MaxSamplingCount Over")
                    break

                if AiSamplingCount >= self.SafeCount: # バッファカウント数が許容量を超えていたら一度データを吸い上げる
                    bufSamplingCount += AiSamplingCount
                    # dataList = self._GetSamplingData(AiSamplingCount, dataList)
                    dataList = self._GetSamplingData(AiSamplingCount, dataList)
                    if self.logger != None:
                        self.logger.debug("bufSamplingCount: {0}".format(bufSamplingCount))

            # サンプリング終了し、バッファ内のデータを吸い上げる -----------------------------------------------------------

            # サンプリング終了処理
            ret = self.caio.StopAi(self.Id)
            if self.logger != None:
                self.logger.debug("StopAi: {0}".format(ret))
            self._getErrorCode(ret)

            # バッファ内のデータをすべて吸い上げる
            # メモリ内格納サンプリング数の取得
            AiSamplingCount      =  0
            ret, AiSamplingCount =  self.caio.GetAiSamplingCount(self.Id, AiSamplingCount)
            bufSamplingCount     += AiSamplingCount
            if self.logger != None:
                self.logger.debug("bufSamplingCount: {0}".format(bufSamplingCount))
            self._getErrorCode(ret)

            # 状態取得
            AiStatus      = 0
            ret, AiStatus = self.caio.GetAiStatus(self.Id, AiStatus)
            if self.logger != None:
                self.logger.debug("GetAiStatus: {0}".format(hex(AiStatus)))
            self._getErrorCode(ret)

            # データ吸い上げ
            # dataList = self._GetSamplingData(AiSamplingCount + 1, dataList)
            dataList        = self._GetSamplingData(AiSamplingCount, dataList)
            self.StartFlg   = False

        except Exception as e:
            self.logger.error(e.msg)
            raise AttributeError()
        finally:
            # Contecデバイスの解放
            ret = self.caio.Exit(self.Id)
            if self.logger != None:
                self.logger.debug("Exit: {0}".format(ret))
            self._getErrorCode(ret)

        # サンプリングデータを PandasDataFrame に整形 ----------------------------------------------------------------

        # データの取り出し
        # データは[ch1,ch2,ch3,ch1,ch2,ch3,.....]のように並んでいる
        nArray = np.zeros((self.Channels, len(dataList) // self.Channels))

        for i in range(0, len(dataList)//self.Channels, 1):
            for j in range(0, self.Channels):
                # nArray[j][i] = dataList[i * self.Channels + j] * 20 / 65536 -10
                nArray[j][i] = dataList[i * self.Channels + j]

        # データ格納用のデータフレームを作成
        dataDict = pd.DataFrame()
        for i in range(self.Channels):
            dataDict["ch{0}".format(i + 1)] = nArray[i]

        if self.logger != None:
            self.logger.debug("DataFrame:\n{0}".format(dataDict))

        return dataDict

    ##
    # @breif comment
    # @param
    # @return
    def _GetSamplingData(self, samplingCount, dataList):
        """
        ### Breif
        comment\n
        ### Args
        * _type_ ArgName :
        \n
        ### Return
        * _type_ value :
        """

        AiSamplingTimes                 =   int(samplingCount)
        AiData                          =   [0.0 for i in range(AiSamplingTimes * self.Channels)]
        ret, AiSamplingTimes, AiData    =   self.caio.GetAiSamplingDataEx(self.Id, AiSamplingTimes, AiData)
        self._getErrorCode(ret)
        dataList.extend(struct.unpack("<%df"%(AiSamplingTimes* self.Channels),AiData))

        return dataList

    ##
    # @breif エラーコードをメッセージに変換する
    # @param int ret : 関数の応答コード
    def _getErrorCode(self, _ret):
        """
        ### Breif
        エラーコードをメッセージに変換する\n
        ### Args
        * _int_ ret : 関数の応答コード
        \n
        """

        if _ret != 0:
            if _ret == 20003:
                error = b"0"
                _ret, error = self.caio.GetErrorString(_ret, error)
                print(error.decode('shift-jis'))

                ret = self.caio.ResetProcess(self.Id)
                if self.logger != None:
                    self.logger.debug("ResetProcess: {0}".format(ret))
                self._getErrorCode(ret)

            else:
                error = b"0"
                _ret, error = self.caio.GetErrorString(_ret, error)
                if self.logger != None:
                    self.logger.error(error.decode(Caio.encode))

#入力レンジ設定
# AioSetAiRange
# ret = self.caio.SetAiRange(Id, 1, C_InOut_Ranges.PM10)
# print("--- SetAiRange ---")
# print(ret)

#転送方式設定
# AioSetAiTransferMode
# ret = self.caio.SetAiTransferMode(Id, 0)
# print("--- SetAiTransferMode ---")
# print(ret)



# 測定開始-----------------------------------



# AioStartAiSync
# StartAiSync




# データ取得停止
# AioStopAi


#データ取得
# AioGetAiSamplingDataEx


# # AioResetAiStatus
# ret = self.caio.ResetAiStatus(Id)
# print("--- ResetAiStatus ---")
# print(ret)


# #AioSetAiScanClock
# ret = self.caio.SetAiScanClock(Id,5000)
# print("--- SetAiScanClock ---")
# print(ret)

# # AioSetAiEventSamplingTimes
# ret = self.caio.SetAiEventSamplingTimes(Id,1000)
# print("--- SetAiEventSamplingTimes ---")
# print(ret)







# # 入力方式設定
# ret = self.caio.SetAiInputMethod(Id, 0)
# print("--- SetAiInputMethod ---")
# print(ret)


# # AioGetAiSamplingData
# times = 1000
# data  = 0
# ret, times, data = self.caio.GetAiSamplingData(Id, times, data)
# print("--- GetAiSamplingData ---")
# print(ret, times, data)



# # 取得サンプリング数がデータ格納配列サイズを上回らないよう調整
# if(AiSamplingCount * m_AiChannels > DATA_MAX)
# {
#     AiSamplingCount = DATA_MAX / m_AiChannels;
# }

