#encoding utf-8
import sys
from ctypes import cdll, windll
from ctypes import *

# Const--------------------------------------------------------

##
# @breif ContecのID定数（この中からIDを選択してください）
class C_AioIDs(object):
    """
    ## ContecのID定数（この中からIDを選択してください）
    ##### AIO000 : "AIO000"
    ##### AIO001 : "AIO001"
    ##### AIO002 : "AIO002"
    ##### AIO003 : "AIO003"
    ##### AIO004 : "AIO004"
    ##### AIO005 : "AIO005"
    """

    AIO000 = "AIO000" #AIO000
    AIO001 = "AIO001" #AIO001
    AIO002 = "AIO002" #AIO002
    AIO003 = "AIO003" #AIO003
    AIO004 = "AIO004" #AIO004
    AIO005 = "AIO005" #AIO005

##
# @breif 外部制御用信号　定数
class C_External_Control(object):
    """
    ## 外部制御用信号　定数
    ##### AIO_AIF_CLOCK :   アナログ入力外部クロック
    ##### AIO_AIF_START :   アナログ入力外部開始トリガ
    ##### AIO_AIF_STOP  :   アナログ入力外部停止トリガ
    ##### AIO_AOF_CLOCK :   アナログ出力外部クロック
    ##### AIO_AOF_START :   アナログ出力外部開始トリガ
    ##### AIO_AOF_STOP  :   アナログ出力外部停止トリガ
    ##### AIO_ALLF      :   上記以外
    """
    # 外部制御信号
    AIO_AIF_CLOCK   = 0            #アナログ入力外部クロック
    AIO_AIF_START   = 1            #アナログ入力外部開始トリガ
    AIO_AIF_STOP    = 2            #アナログ入力外部停止トリガ
    AIO_AOF_CLOCK   = 3            #アナログ出力外部クロック
    AIO_AOF_START   = 4            #アナログ出力外部開始トリガ
    AIO_AOF_STOP    = 5            #アナログ出力外部停止トリガ
    AIO_ALLF        = -1           #上記以外

##
# @breif 入出力レンジをこの中から選択します
class C_InOut_Ranges(object):
    """
    ## 入出力レンジをこの中から選択します
    ##### PM10          :       ±10V
    ##### PM5           :       ±5V
    ##### PM2_5         :       ±2.5V
    ##### PM1_25        :       ±1.25V
    ##### PM1           :       ±1V
    ##### PM0_625       :       ±0.625V
    ##### PM0_5         :       ±0.5V
    ##### PM0_3125      :       ±0.3125V
    ##### PM0_25        :       ±0.25V
    ##### PM0_125       :       ±0.125V
    ##### PM0_1         :       ±0.1V
    ##### PM0_05        :       ±0.05V
    ##### PM0_025       :       ±0.025V
    ##### PM0_0125      :       ±0.0125V
    ##### PM0_01        :       ±0.01V
    ##### P10           :       0～10V
    ##### P5            :       0～5V
    ##### P4_095        :       0～4.095V
    ##### P2_5          :       0～2.5V
    ##### P1_25         :       0～1.25V
    ##### P1            :       0～1V
    ##### P0_5          :       0～0.5V
    ##### P0_25         :       0～0.25V
    ##### P0_1          :       0～0.1V
    ##### P0_05         :       0～0.05V
    ##### P0_025        :       0～0.025V
    ##### P0_0125       :       0～0.0125V
    ##### P0_01         :       0～0.01V
    ##### P_20MA        :       0～20mA
    ##### P4_TO_20MA    :       4～20mA
    ##### P1_TO_5       :       1～5V
    """
    PM10        =0      #±10V
    PM5         =1      #±5V
    PM2_5       =2      #±2.5V
    PM1_25      =3      #±1.25V
    PM1         =4      #±1V
    PM0_625     =5      #±0.625V
    PM0_5       =6      #±0.5V
    PM0_3125    =7      #±0.3125V
    PM0_25      =8      #±0.25V
    PM0_125     =9      #±0.125V
    PM0_1       =10     #±0.1V
    PM0_05      =11     #±0.05V
    PM0_025     =12     #±0.025V
    PM0_0125    =13     #±0.0125V
    PM0_01      =14     #±0.01V
    P10         =50     #0～10V
    P5          =51     #0～5V
    P4_095      =52     #0～4.095V
    P2_5        =53     #0～2.5V
    P1_25       =54     #0～1.25V
    P1          =55     #0～1V
    P0_5        =56     #0～0.5V
    P0_25       =57     #0～0.25V
    P0_1        =58     #0～0.1V
    P0_05       =59     #0～0.05V
    P0_025      =60     #0～0.025V
    P0_0125     =61     #0～0.0125V
    P0_01       =62     #0～0.01V
    P_20MA      =100    #0～20mA
    P4_TO_20MA  =101    #4～20mA
    P1_TO_5     =150    #1～5V

##
# @breif アナログ入力イベント　定数
class C_Ai_Event(object):
    """
    ## アナログ入力イベント　定数
    ##### AIE_START     :    AD変換開始条件成立イベント
    ##### AIE_RPTEND    :    リピート終了イベント
    ##### AIE_END       :    デバイス動作終了イベント
    ##### AIE_DATA_NUM  :    指定サンプリング回数格納イベント
    ##### AIE_DATA_TSF  :    指定転送数毎イベント
    ##### AIE_OFERR     :    オーバーフローイベント
    ##### AIE_SCERR     :    サンプリングクロックエラーイベント
    ##### AIE_ADERR     :    AD変換エラーイベント
    """
    AIE_START       = 0x00000002    #AD変換開始条件成立イベント
    AIE_RPTEND      = 0x00000010    #リピート終了イベント
    AIE_END         = 0x00000020    #デバイス動作終了イベント
    AIE_DATA_NUM    = 0x00000080    #指定サンプリング回数格納イベント
    AIE_DATA_TSF    = 0x00000100    #指定転送数毎イベント
    AIE_OFERR       = 0x00010000    #オーバーフローイベント
    AIE_SCERR       = 0x00020000    #サンプリングクロックエラーイベント
    AIE_ADERR       = 0x00040000    #AD変換エラーイベント

##
# @breif アナログ出力イベント　定数
class C_Ao_Event(object):
    """
    ## アナログ出力イベント　定数
    ##### AOE_START     :  DA変換開始条件成立イベント
    ##### AOE_RPTEND    :  リピート終了イベント
    ##### AOE_END       :  デバイス動作終了イベント
    ##### AOE_DATA_NUM  :  指定サンプリング回数出力イベント
    ##### AOE_DATA_TSF  :  指定転送数毎イベント
    ##### AOE_SCERR     :  サンプリングクロックエラーイベント
    ##### AOE_DAERR     :  DA変換エラーイベント
    """

    AOE_START       = 0x00000002    #DA変換開始条件成立イベント
    AOE_RPTEND      = 0x00000010    #リピート終了イベント
    AOE_END         = 0x00000020    #デバイス動作終了イベント
    AOE_DATA_NUM    = 0x00000080    #指定サンプリング回数出力イベント
    AOE_DATA_TSF    = 0x00000100    #指定転送数毎イベント
    AOE_SCERR       = 0x00020000    #サンプリングクロックエラーイベント
    AOE_DAERR       = 0x00040000    #DA変換エラーイベント

##
# @breif カウンタイベント　定数
class C_Counter(object):
    """
    ## カウンタイベント　定数
    ##### CNTE_DATA_NUM :   比較カウント一致イベント
    ##### CNTE_ORERR    :   カウントオーバーランイベント
    ##### CNTE_ERR      :   カウンタ動作エラー
    """

    CNTE_DATA_NUM   = 0x00000010    #比較カウント一致イベント
    CNTE_ORERR      = 0x00010000    #カウントオーバーランイベント
    CNTE_ERR        = 0x00020000    #カウンタ動作エラー

##
# @breif タイマーイベント　定数
class C_Timer(object):
    """
    ## タイマーイベント　定数
    ##### TME_INT :     インターバル成立イベント
    """

    TME_INT = 0x00000001    #インターバル成立イベント

##
# @breif アナログ入力ステータス　定数
class C_Ai_States(object):
    """
    ## アナログ入力ステータス　定数
    ##### AIS_BUSY      :   デバイス動作中
    ##### AIS_START_TRG :   開始トリガ待ち
    ##### AIS_DATA_NUM  :   指定サンプリング回数格納
    ##### AIS_OFERR     :   オーバーフロー
    ##### AIS_SCERR     :   サンプリングクロックエラー
    ##### AIS_AIERR     :   AD変換エラー
    ##### AIS_DRVERR    :   ドライバスペックエラー
    """

    AIS_BUSY        = 0x00000001    #デバイス動作中
    AIS_START_TRG   = 0x00000002    #開始トリガ待ち
    AIS_DATA_NUM    = 0x00000010    #指定サンプリング回数格納
    AIS_OFERR       = 0x00010000    #オーバーフロー
    AIS_SCERR       = 0x00020000    #サンプリングクロックエラー
    AIS_AIERR       = 0x00040000    #AD変換エラー
    AIS_DRVERR      = 0x00080000    #ドライバスペックエラー

##
# @breif アナログ出力ステータス　定数
class C_Ao_States(object):
    """
    ## アナログ出力ステータス　定数
    ##### AOS_BUSY      :   デバイス動作中
    ##### AOS_START_TRG :   開始トリガ待ち
    ##### AOS_DATA_NUM  :   指定サンプリング回数出力
    ##### AOS_SCERR     :   サンプリングクロックエラー
    ##### AOS_AOERR     :   DA変換エラー
    ##### AOS_DRVERR    :   ドライバスペックエラー
    """

    AOS_BUSY        = 0x00000001    #デバイス動作中
    AOS_START_TRG   = 0x00000002    #開始トリガ待ち
    AOS_DATA_NUM    = 0x00000010    #指定サンプリング回数出力
    AOS_SCERR       = 0x00020000    #サンプリングクロックエラー
    AOS_AOERR       = 0x00040000    #DA変換エラー
    AOS_DRVERR      = 0x00080000    #ドライバスペックエラー

##
# @breif カウンタステータス　定数
class C_Counter_States(object):
    """
    ## カウンタステータス　定数

    ##### CNTS_BUSY      :      カウンタ動作中
    ##### CNTS_DATA_NUM  :      比較カウント一致
    ##### CNTS_ORERR     :      オーバーラン
    ##### CNTS_ERR       :      カウンタ動作エラー
    """

    CNTS_BUSY       = 0x00000001    #カウンタ動作中
    CNTS_DATA_NUM   = 0x00000010    #比較カウント一致
    CNTS_ORERR      = 0x00010000    #オーバーラン
    CNTS_ERR        = 0x00020000    #カウンタ動作エラー

##
# @breif アナログ入力メッセージ　定数
class C_Ai_Message(object):
    """
    ## アナログ入力メッセージ　定数

    ##### AIOM_AIE_START    :   AD変換開始条件成立イベント
    ##### AIOM_AIE_RPTEND   :   リピート終了イベント
    ##### AIOM_AIE_END      :   デバイス動作終了イベント
    ##### AIOM_AIE_DATA_NUM :   指定サンプリング回数格納イベント
    ##### AIOM_AIE_DATA_TSF :   指定転送数毎イベント
    ##### AIOM_AIE_OFERR    :   オーバーフローイベント
    ##### AIOM_AIE_SCERR    :   サンプリングクロックエラーイベント
    ##### AIOM_AIE_ADERR    :   AD変換エラーイベント
    """

    AIOM_AIE_START      = 0x1000        #AD変換開始条件成立イベント
    AIOM_AIE_RPTEND     = 0x1001        #リピート終了イベント
    AIOM_AIE_END        = 0x1002        #デバイス動作終了イベント
    AIOM_AIE_DATA_NUM   = 0x1003        #指定サンプリング回数格納イベント
    AIOM_AIE_DATA_TSF   = 0x1007        #指定転送数毎イベント
    AIOM_AIE_OFERR      = 0x1004        #オーバーフローイベント
    AIOM_AIE_SCERR      = 0x1005        #サンプリングクロックエラーイベント
    AIOM_AIE_ADERR      = 0x1006        #AD変換エラーイベント

##
# @breif アナログ出力メッセージ　定数
class C_Ao_Message(object):
    """
    ## アナログ出力メッセージ　定数
    ##### AIOM_AOE_START     :      DA変換開始条件成立イベント
    ##### AIOM_AOE_RPTEND    :      リピート終了イベント
    ##### AIOM_AOE_END       :      デバイス動作終了イベント
    ##### AIOM_AOE_DATA_NUM  :      指定サンプリング回数出力イベント
    ##### AIOM_AOE_DATA_TSF  :      指定転送数毎イベント
    ##### AIOM_AOE_SCERR     :      サンプリングクロックエラーイベント
    ##### AIOM_AOE_DAERR     :      DA変換エラーイベント
    """

    AIOM_AOE_START      = 0x1020        #DA変換開始条件成立イベント
    AIOM_AOE_RPTEND     = 0x1021        #リピート終了イベント
    AIOM_AOE_END        = 0x1022        #デバイス動作終了イベント
    AIOM_AOE_DATA_NUM   = 0x1023        #指定サンプリング回数出力イベント
    AIOM_AOE_DATA_TSF   = 0x1027        #指定転送数毎イベント
    AIOM_AOE_SCERR      = 0x1025        #サンプリングクロックエラーイベント
    AIOM_AOE_DAERR      = 0x1026        #DA変換エラーイベント

##
# @breif カウンタメッセージ　定数
class C_Counter_Message(object):
    """
    ## カウンタメッセージ　定数
    ##### AIOM_CNTE_DATA_NUM  :  比較カウント一致イベント
    ##### AIOM_CNTE_ORERR     :  カウントオーバーランイベント
    ##### AIOM_CNTE_ERR       :  カウント動作エラーイベント
    """

    AIOM_CNTE_DATA_NUM  = 0x1042        #比較カウント一致イベント
    AIOM_CNTE_ORERR     = 0x1043        #カウントオーバーランイベント
    AIOM_CNTE_ERR       = 0x1044        #カウント動作エラーイベント

##
# @breif タイマーメッセージ　定数
class C_Timer_Message(object):
    """
    ## タイマーメッセージ　定数
    #### AIOM_TME_INT  : インターバル成立イベント
    """

    AIOM_TME_INT    = 0x1060        #インターバル成立イベント

##
# @breif デバイスカウンター用メッセージ　定数
class C_DeviceCounter_Message(object):
    """
    ## デバイスカウンター用メッセージ　定数
    ##### AIOM_CNTM_COUNTUP_CH0    :   カウントアップ、チャネル番号0
    ##### AIOM_CNTM_COUNTUP_CH1    :   カウントアップ、チャネル番号1
    ##### AIOM_CNTM_TIME_UP        :   タイムアップ
    ##### AIOM_CNTM_COUNTER_ERROR  :   カウンタエラー
    ##### AIOM_CNTM_CARRY_BORROW   :   キャリー／ボロー
    """

    AIOM_CNTM_COUNTUP_CH0       = 0x1070        # カウントアップ、チャネル番号0
    AIOM_CNTM_COUNTUP_CH1       = 0x1071        # カウントアップ、チャネル番号1
    AIOM_CNTM_TIME_UP           = 0x1090        #タイムアップ
    AIOM_CNTM_COUNTER_ERROR     = 0x1091        #カウンタエラー
    AIOM_CNTM_CARRY_BORROW      = 0x1092        #キャリー／ボロー

##
# @breif アナログ入力添付データ　定数
class C_Ai_AttachedData(object):
    """
    ## アナログ入力添付データ　定数
    ##### AIAT_AI    :   アナログ入力付属情報
    ##### AIAT_AO0   :   アナログ出力データ
    ##### AIAT_DIO0  :   デジタル入出力データ
    ##### AIAT_CNT0  :   カウンタチャネル０データ
    ##### AIAT_CNT1  :   カウンタチャネル１データ
    """

    AIAT_AI     = 0x00000001    #アナログ入力付属情報
    AIAT_AO0    = 0x00000100    #アナログ出力データ
    AIAT_DIO0   = 0x00010000    #デジタル入出力データ
    AIAT_CNT0   = 0x01000000    #カウンタチャネル０データ
    AIAT_CNT1   = 0x02000000    #カウンタチャネル１データ

##
# @breif カウンタ動作モード　定数
class C_Counter_OperationMode(object):
    """
    ## カウンタ動作モード　定数
    ##### CNT_LOADPRESET    :  プリセットカウント値のロード
    ##### CNT_LOADCOMP      :  比較カウント値のロード
    """

    CNT_LOADPRESET  = 0x0000001    #プリセットカウント値のロード
    CNT_LOADCOMP    = 0x0000002    #比較カウント値のロード

##
# @breif イベントコントローラ接続先信号　定数
class C_EvConect_Destination(object):
    """
    ## イベントコントローラ接続先信号　定数
    ##### AIOECU_DEST_AI_CLK      :   アナログ入力サンプリングクロック
    ##### AIOECU_DEST_AI_START    :   アナログ入力変換開始信号
    ##### AIOECU_DEST_AI_STOP     :   アナログ入力変換停止信号
    ##### AIOECU_DEST_AO_CLK      :   アナログ出力サンプリングクロック
    ##### AIOECU_DEST_AO_START    :   アナログ出力変換開始信号
    ##### AIOECU_DEST_AO_STOP     :   アナログ出力変換停止信号
    ##### AIOECU_DEST_CNT0_UPCLK  :   カウンタ０アップクロック信号
    ##### AIOECU_DEST_CNT1_UPCLK  :   カウンタ１アップクロック信号
    ##### AIOECU_DEST_CNT0_START  :   カウンタ０、タイマ０動作開始信号
    ##### AIOECU_DEST_CNT1_START  :   カウンタ１、タイマ１動作開始信号
    ##### AIOECU_DEST_CNT0_STOP   :   カウンタ０、タイマ０動作停止信号
    ##### AIOECU_DEST_CNT1_STOP   :   カウンタ１、タイマ１動作停止信号
    ##### AIOECU_DEST_MASTER1     :   同期バスマスタ信号１
    ##### AIOECU_DEST_MASTER2     :   同期バスマスタ信号２
    ##### AIOECU_DEST_MASTER3     :   同期バスマスタ信号３
    """

    AIOECU_DEST_AI_CLK      = 4            #アナログ入力サンプリングクロック
    AIOECU_DEST_AI_START    = 0            #アナログ入力変換開始信号
    AIOECU_DEST_AI_STOP     = 2            #アナログ入力変換停止信号
    AIOECU_DEST_AO_CLK      = 36           #アナログ出力サンプリングクロック
    AIOECU_DEST_AO_START    = 32           #アナログ出力変換開始信号
    AIOECU_DEST_AO_STOP     = 34           #アナログ出力変換停止信号
    AIOECU_DEST_CNT0_UPCLK  = 134          #カウンタ０アップクロック信号
    AIOECU_DEST_CNT1_UPCLK  = 135          #カウンタ１アップクロック信号
    AIOECU_DEST_CNT0_START  = 128          #カウンタ０、タイマ０動作開始信号
    AIOECU_DEST_CNT1_START  = 129          #カウンタ１、タイマ１動作開始信号
    AIOECU_DEST_CNT0_STOP   = 130          #カウンタ０、タイマ０動作停止信号
    AIOECU_DEST_CNT1_STOP   = 131          #カウンタ１、タイマ１動作停止信号
    AIOECU_DEST_MASTER1     = 104          #同期バスマスタ信号１
    AIOECU_DEST_MASTER2     = 105          #同期バスマスタ信号２
    AIOECU_DEST_MASTER3     = 106          #同期バスマスタ信号３

##
# @breif イベントコントローラ接続元信号　定数
class C_EvConect_Souce(object):
    """
    ## イベントコントローラ接続元信号　定数
    ##### AIOECU_SRC_OPEN           :  未接続
    ##### AIOECU_SRC_AI_CLK         :  アナログ入力内部クロック信号
    ##### AIOECU_SRC_AI_EXTCLK      :  アナログ入力外部クロック信号
    ##### AIOECU_SRC_AI_TRGSTART    :  アナログ入力外部トリガ開始信号
    ##### AIOECU_SRC_AI_LVSTART     :  アナログ入力レベルトリガ開始信号
    ##### AIOECU_SRC_AI_STOP        :  アナログ入力変換回数終了信号（遅延なし）
    ##### AIOECU_SRC_AI_STOP_DELAY  :  アナログ入力変換回数終了信号（遅延あり）
    ##### AIOECU_SRC_AI_LVSTOP      :  アナログ入力レベルトリガ停止信号
    ##### AIOECU_SRC_AI_TRGSTOP     :  アナログ入力外部トリガ停止信号
    ##### AIOECU_SRC_AO_CLK         :  アナログ出力内部クロック信号
    ##### AIOECU_SRC_AO_EXTCLK      :  アナログ出力外部クロック信号
    ##### AIOECU_SRC_AO_TRGSTART    :  アナログ出力外部トリガ開始信号
    ##### AIOECU_SRC_AO_STOP_FIFO   :  アナログ出力指定回数出力終了信号（FIFO使用）
    ##### AIOECU_SRC_AO_STOP_RING   :  アナログ出力指定回数出力終了信号（RING使用）
    ##### AIOECU_SRC_AO_TRGSTOP     :  アナログ出力外部トリガ停止信号
    ##### AIOECU_SRC_CNT0_UPCLK     :  カウンタ０アップクロック信号
    ##### AIOECU_SRC_CNT1_UPCLK     :  カウンタ１アップクロック信号
    ##### AIOECU_SRC_CNT0_CMP       :  カウンタ０比較カウント一致
    ##### AIOECU_SRC_CNT1_CMP       :  カウンタ１比較カウント一致
    ##### AIOECU_SRC_SLAVE1         :  同期バススレーブ信号１
    ##### AIOECU_SRC_SLAVE2         :  同期バススレーブ信号２
    ##### AIOECU_SRC_SLAVE3         :  同期バススレーブ信号３
    ##### AIOECU_SRC_START          :  Ai Ao Cnt Tmソフトウェア開始信号
    ##### AIOECU_SRC_STOP           :  Ai Ao Cnt Tmソフトウェア停止信号
    """

    AIOECU_SRC_OPEN             = -1            #未接続
    AIOECU_SRC_AI_CLK           = 4             #アナログ入力内部クロック信号
    AIOECU_SRC_AI_EXTCLK        = 146           #アナログ入力外部クロック信号
    AIOECU_SRC_AI_TRGSTART      = 144           #アナログ入力外部トリガ開始信号
    AIOECU_SRC_AI_LVSTART       = 28            #アナログ入力レベルトリガ開始信号
    AIOECU_SRC_AI_STOP          = 17            #アナログ入力変換回数終了信号（遅延なし）
    AIOECU_SRC_AI_STOP_DELAY    = 18            #アナログ入力変換回数終了信号（遅延あり）
    AIOECU_SRC_AI_LVSTOP        = 29            #アナログ入力レベルトリガ停止信号
    AIOECU_SRC_AI_TRGSTOP       = 145           #アナログ入力外部トリガ停止信号
    AIOECU_SRC_AO_CLK           = 66            #アナログ出力内部クロック信号
    AIOECU_SRC_AO_EXTCLK        = 149           #アナログ出力外部クロック信号
    AIOECU_SRC_AO_TRGSTART      = 147           #アナログ出力外部トリガ開始信号
    AIOECU_SRC_AO_STOP_FIFO     = 352           #アナログ出力指定回数出力終了信号（FIFO使用）
    AIOECU_SRC_AO_STOP_RING     = 80            #アナログ出力指定回数出力終了信号（RING使用）
    AIOECU_SRC_AO_TRGSTOP       = 148           #アナログ出力外部トリガ停止信号
    AIOECU_SRC_CNT0_UPCLK       = 150           #カウンタ０アップクロック信号
    AIOECU_SRC_CNT1_UPCLK       = 152           #カウンタ１アップクロック信号
    AIOECU_SRC_CNT0_CMP         = 288           #カウンタ０比較カウント一致
    AIOECU_SRC_CNT1_CMP         = 289           #カウンタ１比較カウント一致
    AIOECU_SRC_SLAVE1           = 136           #同期バススレーブ信号１
    AIOECU_SRC_SLAVE2           = 137           #同期バススレーブ信号２
    AIOECU_SRC_SLAVE3           = 138           #同期バススレーブ信号３
    AIOECU_SRC_START            = 384           #Ai Ao Cnt Tmソフトウェア開始信号
    AIOECU_SRC_STOP             = 385           #Ai Ao Cnt Tmソフトウェア停止信号



# DllImport-----------------------------------------------------
# DllからのImportを実行
# --------------------------------------------------------------


# ProtoTypeFunctions--------------------------------------------

contecDll = windll.LoadLibrary('./caio.dll')

AioInit = contecDll.AioInit # long WINAPI AioInit(char* DeviceName, short* Id);
AioExit = contecDll.AioExit # long WINAPI AioExit(short Id);
AioResetDevice = contecDll.AioResetDevice # long WINAPI AioResetDevice(short Id);
AioGetErrorString = contecDll.AioGetErrorString # long WINAPI AioGetErrorString(long ErrorCode, char* ErrorString);
AioQueryDeviceName = contecDll.AioQueryDeviceName # long WINAPI AioQueryDeviceName(short Index, char* DeviceName, char* Device);
AioGetDeviceType = contecDll.AioGetDeviceType # long WINAPI AioGetDeviceType(char* Device, short* DeviceType);
AioSetControlFilter = contecDll.AioSetControlFilter # long WINAPI AioSetControlFilter(short Id, short Signal, float Value);
AioGetControlFilter = contecDll.AioGetControlFilter # long WINAPI AioGetControlFilter(short Id, short Signal, float* Value);
AioResetProcess = contecDll.AioResetProcess # long WINAPI AioResetProcess(short Id);
AioSingleAi = contecDll.AioSingleAi # long WINAPI AioSingleAi(short Id, short AiChannel, long* AiData);
AioSingleAiEx = contecDll.AioSingleAiEx # long WINAPI AioSingleAiEx(short Id, short AiChannel, float* AiData);
AioMultiAi = contecDll.AioMultiAi # long WINAPI AioMultiAi(short Id, short AiChannels, long* AiData);
AioMultiAiEx = contecDll.AioMultiAiEx # long WINAPI AioMultiAiEx(short Id, short AiChannels, float* AiData);
AioGetAiResolution = contecDll.AioGetAiResolution # long WINAPI AioGetAiResolution(short Id, short* AiResolution);
AioSetAiInputMethod = contecDll.AioSetAiInputMethod # long WINAPI AioSetAiInputMethod(short Id, short AiInputMethod);
AioGetAiInputMethod = contecDll.AioGetAiInputMethod # long WINAPI AioGetAiInputMethod(short Id, short* AiInputMethod);
AioGetAiMaxChannels = contecDll.AioGetAiMaxChannels # long WINAPI AioGetAiMaxChannels(short Id, short* AiMaxChannels);
AioSetAiChannel = contecDll.AioSetAiChannel # long WINAPI AioSetAiChannel(short Id, short AiChannel, short Enabled);
AioGetAiChannel = contecDll.AioGetAiChannel # long WINAPI AioGetAiChannel(short Id, short AiChannel, short* Enabled);
AioSetAiChannels = contecDll.AioSetAiChannels # long WINAPI AioSetAiChannels(short Id, short AiChannels);
AioGetAiChannels = contecDll.AioGetAiChannels # long WINAPI AioGetAiChannels(short Id, short* AiChannels);
AioSetAiChannelSequence = contecDll.AioSetAiChannelSequence # long WINAPI AioSetAiChannelSequence(short Id, short AiSequence, short AiChannel);
AioGetAiChannelSequence = contecDll.AioGetAiChannelSequence # long WINAPI AioGetAiChannelSequence(short Id, short AiSequence, short* AiChannel);
AioSetAiRange = contecDll.AioSetAiRange # long WINAPI AioSetAiRange(short Id, short AiChannel, short AiRange);
AioSetAiRangeAll = contecDll.AioSetAiRangeAll # long WINAPI AioSetAiRangeAll(short Id, short AiRange);
AioGetAiRange = contecDll.AioGetAiRange # long WINAPI AioGetAiRange(short Id, short AiChannel, short* AiRange);
AioSetAiTransferMode = contecDll.AioSetAiTransferMode # long WINAPI AioSetAiTransferMode(short Id, short AiTransferMode);
AioGetAiTransferMode = contecDll.AioGetAiTransferMode # long WINAPI AioGetAiTransferMode(short Id, short* AiTransferMode);
AioSetAiDeviceBufferMode = contecDll.AioSetAiDeviceBufferMode # long WINAPI AioSetAiDeviceBufferMode(short Id, short AiDeviceBufferMode);
AioGetAiDeviceBufferMode = contecDll.AioGetAiDeviceBufferMode # long WINAPI AioGetAiDeviceBufferMode(short Id, short* AiDeviceBufferMode);
AioSetAiMemorySize = contecDll.AioSetAiMemorySize # long WINAPI AioSetAiMemorySize(short Id, long AiMemorySize);
AioGetAiMemorySize = contecDll.AioGetAiMemorySize # long WINAPI AioGetAiMemorySize(short Id, long* AiMemorySize);
AioSetAiTransferData = contecDll.AioSetAiTransferData # long WINAPI AioSetAiTransferData(short Id, long DataNumber, long* Buffer);
AioSetAiAttachedData = contecDll.AioSetAiAttachedData # long WINAPI AioSetAiAttachedData(short Id, long AttachedData);
AioGetAiSamplingDataSize = contecDll.AioGetAiSamplingDataSize # long WINAPI AioGetAiSamplingDataSize(short Id, short* DataSize);
AioSetAiMemoryType = contecDll.AioSetAiMemoryType # long WINAPI AioSetAiMemoryType(short Id, short AiMemoryType);
AioGetAiMemoryType = contecDll.AioGetAiMemoryType # long WINAPI AioGetAiMemoryType(short Id, short* AiMemoryType);
AioSetAiRepeatTimes = contecDll.AioSetAiRepeatTimes # long WINAPI AioSetAiRepeatTimes(short Id, long AiRepeatTimes);
AioGetAiRepeatTimes = contecDll.AioGetAiRepeatTimes # long WINAPI AioGetAiRepeatTimes(short Id, long* AiRepeatTimes);
AioSetAiClockType = contecDll.AioSetAiClockType # long WINAPI AioSetAiClockType(short Id, short AiClockType);
AioGetAiClockType = contecDll.AioGetAiClockType # long WINAPI AioGetAiClockType(short Id, short* AiClockType);
AioSetAiSamplingClock = contecDll.AioSetAiSamplingClock # long WINAPI AioSetAiSamplingClock(short Id, float AiSamplingClock);
AioGetAiSamplingClock = contecDll.AioGetAiSamplingClock # long WINAPI AioGetAiSamplingClock(short Id, float* AiSamplingClock);
AioSetAiScanClock = contecDll.AioSetAiScanClock # long WINAPI AioSetAiScanClock(short Id, float AiScanClock);
AioGetAiScanClock = contecDll.AioGetAiScanClock # long WINAPI AioGetAiScanClock(short Id, float* AiScanClock);
AioSetAiClockEdge = contecDll.AioSetAiClockEdge # long WINAPI AioSetAiClockEdge(short Id, short AoClockEdge);
AioGetAiClockEdge = contecDll.AioGetAiClockEdge # long WINAPI AioGetAiClockEdge(short Id, short* AoClockEdge);
AioSetAiStartTrigger = contecDll.AioSetAiStartTrigger # long WINAPI AioSetAiStartTrigger(short Id, short AiStartTrigger);
AioGetAiStartTrigger = contecDll.AioGetAiStartTrigger # long WINAPI AioGetAiStartTrigger(short Id, short* AiStartTrigger);
AioSetAiStartLevel = contecDll.AioSetAiStartLevel # long WINAPI AioSetAiStartLevel(short Id, short AiChannel, long AiStartLevel, short AiDirection);
AioSetAiStartLevelEx = contecDll.AioSetAiStartLevelEx # long WINAPI AioSetAiStartLevelEx(short Id, short AiChannel, float AiStartLevel, short AiDirection);
AioGetAiStartLevel = contecDll.AioGetAiStartLevel # long WINAPI AioGetAiStartLevel(short Id, short AiChannel, long* AiStartLevel, short* AiDirection);
AioGetAiStartLevelEx = contecDll.AioGetAiStartLevelEx # long WINAPI AioGetAiStartLevelEx(short Id, short AiChannel, float* AiStartLevel, short* AiDirection);
AioSetAiStartInRange = contecDll.AioSetAiStartInRange # long WINAPI AioSetAiStartInRange(short Id, short AiChannel, long Level1, long Level2, long StateTimes);
AioSetAiStartInRangeEx = contecDll.AioSetAiStartInRangeEx # long WINAPI AioSetAiStartInRangeEx(short Id, short AiChannel, float Level1, float Level2, long StateTimes);
AioGetAiStartInRange = contecDll.AioGetAiStartInRange # long WINAPI AioGetAiStartInRange(short Id, short AiChannel, long* Level1, long* Level2, long* StateTimes);
AioGetAiStartInRangeEx = contecDll.AioGetAiStartInRangeEx # long WINAPI AioGetAiStartInRangeEx(short Id, short AiChannel, float* Level1, float* Level2, long* StateTimes);
AioSetAiStartOutRange = contecDll.AioSetAiStartOutRange # long WINAPI AioSetAiStartOutRange(short Id, short AiChannel, long Level1, long Level2, long StateTimes);
AioSetAiStartOutRangeEx = contecDll.AioSetAiStartOutRangeEx # long WINAPI AioSetAiStartOutRangeEx(short Id, short AiChannel, float Level1, float Level2, long StateTimes);
AioGetAiStartOutRange = contecDll.AioGetAiStartOutRange # long WINAPI AioGetAiStartOutRange(short Id, short AiChannel, long* Level1, long* Level2, long* StateTimes);
AioGetAiStartOutRangeEx = contecDll.AioGetAiStartOutRangeEx # long WINAPI AioGetAiStartOutRangeEx(short Id, short AiChannel, float* Level1, float* Level2, long* StateTimes);
AioSetAiStopTrigger = contecDll.AioSetAiStopTrigger # long WINAPI AioSetAiStopTrigger(short Id, short AiStopTrigger);
AioGetAiStopTrigger = contecDll.AioGetAiStopTrigger # long WINAPI AioGetAiStopTrigger(short Id, short* AiStopTrigger);
AioSetAiStopTimes = contecDll.AioSetAiStopTimes # long WINAPI AioSetAiStopTimes(short Id, long AiStopTimes);
AioGetAiStopTimes = contecDll.AioGetAiStopTimes # long WINAPI AioGetAiStopTimes(short Id, long* AiStopTimes);
AioSetAiStopLevel = contecDll.AioSetAiStopLevel # long WINAPI AioSetAiStopLevel(short Id, short AiChannel, long AiStopLevel, short AiDirection);
AioSetAiStopLevelEx = contecDll.AioSetAiStopLevelEx # long WINAPI AioSetAiStopLevelEx(short Id, short AiChannel, float AiStopLevel, short AiDirection);
AioGetAiStopLevel = contecDll.AioGetAiStopLevel # long WINAPI AioGetAiStopLevel(short Id, short AiChannel, long* AiStopLevel, short* AiDirection);
AioGetAiStopLevelEx = contecDll.AioGetAiStopLevelEx # long WINAPI AioGetAiStopLevelEx(short Id, short AiChannel, float* AiStopLevel, short* AiDirection);
AioSetAiStopInRange = contecDll.AioSetAiStopInRange # long WINAPI AioSetAiStopInRange(short Id, short AiChannel, long Level1, long Level2, long StateTimes);
AioSetAiStopInRangeEx = contecDll.AioSetAiStopInRangeEx # long WINAPI AioSetAiStopInRangeEx(short Id, short AiChannel, float Level1, float Level2, long StateTimes);
AioGetAiStopInRange = contecDll.AioGetAiStopInRange # long WINAPI AioGetAiStopInRange(short Id, short AiChannel, long* Level1, long* Level2, long* StateTimes);
AioGetAiStopInRangeEx = contecDll.AioGetAiStopInRangeEx # long WINAPI AioGetAiStopInRangeEx(short Id, short AiChannel, float* Level1, float* Level2, long* StateTimes);
AioSetAiStopOutRange = contecDll.AioSetAiStopOutRange # long WINAPI AioSetAiStopOutRange(short Id, short AiChannel, long Level1, long Level2, long StateTimes);
AioSetAiStopOutRangeEx = contecDll.AioSetAiStopOutRangeEx # long WINAPI AioSetAiStopOutRangeEx(short Id, short AiChannel, float Level1, float Level2, long StateTimes);
AioGetAiStopOutRange = contecDll.AioGetAiStopOutRange # long WINAPI AioGetAiStopOutRange(short Id, short AiChannel, long* Level1, long* Level2, long* StateTimes);
AioGetAiStopOutRangeEx = contecDll.AioGetAiStopOutRangeEx # long WINAPI AioGetAiStopOutRangeEx(short Id, short AiChannel, float* Level1, float* Level2, long* StateTimes);
AioSetAiStopDelayTimes = contecDll.AioSetAiStopDelayTimes # long WINAPI AioSetAiStopDelayTimes(short Id, long AiStopDelayTimes);
AioGetAiStopDelayTimes = contecDll.AioGetAiStopDelayTimes # long WINAPI AioGetAiStopDelayTimes(short Id, long* AiStopDelayTimes);
AioSetAiEvent = contecDll.AioSetAiEvent # long WINAPI AioSetAiEvent(short Id, HWND hWnd, long AiEvent);
AioGetAiEvent = contecDll.AioGetAiEvent # long WINAPI AioGetAiEvent(short Id, HWND* hWnd, long* AiEvent);
AioSetAiEventSamplingTimes = contecDll.AioSetAiEventSamplingTimes # long WINAPI AioSetAiEventSamplingTimes(short Id, long AiSamplingTimes);
AioGetAiEventSamplingTimes = contecDll.AioGetAiEventSamplingTimes # long WINAPI AioGetAiEventSamplingTimes(short Id, long* AiSamplingTimes);
AioSetAiEventTransferTimes = contecDll.AioSetAiEventTransferTimes # long WINAPI AioSetAiEventTransferTimes(short Id, long AiTransferTimes);
AioGetAiEventTransferTimes = contecDll.AioGetAiEventTransferTimes # long WINAPI AioGetAiEventTransferTimes(short Id, long* AiTransferTimes);
AioStartAi = contecDll.AioStartAi # long WINAPI AioStartAi(short Id);
AioStartAiSync = contecDll.AioStartAiSync # long WINAPI AioStartAiSync(short Id, long TimeOut);
AioStopAi = contecDll.AioStopAi # long WINAPI AioStopAi(short Id);
AioGetAiStatus = contecDll.AioGetAiStatus # long WINAPI AioGetAiStatus(short Id, long* AiStatus);
AioGetAiSamplingCount = contecDll.AioGetAiSamplingCount # long WINAPI AioGetAiSamplingCount(short Id, long* AiSamplingCount);
AioGetAiStopTriggerCount = contecDll.AioGetAiStopTriggerCount # long WINAPI AioGetAiStopTriggerCount(short Id, long* AiStopTriggerCount);
AioGetAiTransferCount = contecDll.AioGetAiTransferCount # long WINAPI AioGetAiTransferCount(short Id, long* AiTransferCount);
AioGetAiTransferLap = contecDll.AioGetAiTransferLap # long WINAPI AioGetAiTransferLap(short Id, long* Lap);
AioGetAiStopTriggerTransferCount = contecDll.AioGetAiStopTriggerTransferCount # long WINAPI AioGetAiStopTriggerTransferCount(short Id, long* Count);
AioGetAiRepeatCount = contecDll.AioGetAiRepeatCount # long WINAPI AioGetAiRepeatCount(short Id, long* AiRepeatCount);
AioGetAiSamplingData = contecDll.AioGetAiSamplingData # long WINAPI AioGetAiSamplingData(short Id, long* AiSamplingTimes, long* AiData);
AioGetAiSamplingDataEx = contecDll.AioGetAiSamplingDataEx # long WINAPI AioGetAiSamplingDataEx(short Id, long* AiSamplingTimes, float* AiData);
AioResetAiStatus = contecDll.AioResetAiStatus # long WINAPI AioResetAiStatus(short Id);
AioResetAiMemory = contecDll.AioResetAiMemory # long WINAPI AioResetAiMemory(short Id);
AioSingleAo = contecDll.AioSingleAo # long WINAPI AioSingleAo(short Id, short AoChannel, long AoData);
AioSingleAoEx = contecDll.AioSingleAoEx # long WINAPI AioSingleAoEx(short Id, short AoChannel, float AoData);
AioMultiAo = contecDll.AioMultiAo # long WINAPI AioMultiAo(short Id, short AoChannels, long* AoData);
AioMultiAoEx = contecDll.AioMultiAoEx # long WINAPI AioMultiAoEx(short Id, short AoChannels, float* AoData);
AioGetAoResolution = contecDll.AioGetAoResolution # long WINAPI AioGetAoResolution(short Id, short* AoResolution);
AioSetAoChannels = contecDll.AioSetAoChannels # long WINAPI AioSetAoChannels(short Id, short AoChannels);
AioGetAoChannels = contecDll.AioGetAoChannels # long WINAPI AioGetAoChannels(short Id, short* AoChannels);
AioGetAoMaxChannels = contecDll.AioGetAoMaxChannels # long WINAPI AioGetAoMaxChannels(short Id, short* AoMaxChannels);
AioSetAoRange = contecDll.AioSetAoRange # long WINAPI AioSetAoRange(short Id, short AoChannel, short AoRange);
AioSetAoRangeAll = contecDll.AioSetAoRangeAll # long WINAPI AioSetAoRangeAll(short Id, short AoRange);
AioGetAoRange = contecDll.AioGetAoRange # long WINAPI AioGetAoRange(short Id, short AoChannel, short* AoRange);
AioSetAoTransferMode = contecDll.AioSetAoTransferMode # long WINAPI AioSetAoTransferMode(short Id, short AoTransferMode);
AioGetAoTransferMode = contecDll.AioGetAoTransferMode # long WINAPI AioGetAoTransferMode(short Id, short* AoTransferMode);
AioSetAoDeviceBufferMode = contecDll.AioSetAoDeviceBufferMode # long WINAPI AioSetAoDeviceBufferMode(short Id, short AoDeviceBufferMode);
AioGetAoDeviceBufferMode = contecDll.AioGetAoDeviceBufferMode # long WINAPI AioGetAoDeviceBufferMode(short Id, short* AoDeviceBufferMode);
AioSetAoMemorySize = contecDll.AioSetAoMemorySize # long WINAPI AioSetAoMemorySize(short Id, long AoMemorySize);
AioGetAoMemorySize = contecDll.AioGetAoMemorySize # long WINAPI AioGetAoMemorySize(short Id, long* AoMemorySize);
AioSetAoTransferData = contecDll.AioSetAoTransferData # long WINAPI AioSetAoTransferData(short Id, long DataNumber, long* Buffer);
AioGetAoSamplingDataSize = contecDll.AioGetAoSamplingDataSize # long WINAPI AioGetAoSamplingDataSize(short Id, short* DataSize);
AioSetAoMemoryType = contecDll.AioSetAoMemoryType # long WINAPI AioSetAoMemoryType(short Id, short AoMemoryType);
AioGetAoMemoryType = contecDll.AioGetAoMemoryType # long WINAPI AioGetAoMemoryType(short Id, short* AoMemoryType);
AioSetAoRepeatTimes = contecDll.AioSetAoRepeatTimes # long WINAPI AioSetAoRepeatTimes(short Id, long AoRepeatTimes);
AioGetAoRepeatTimes = contecDll.AioGetAoRepeatTimes # long WINAPI AioGetAoRepeatTimes(short Id, long* AoRepeatTimes);
AioSetAoClockType = contecDll.AioSetAoClockType # long WINAPI AioSetAoClockType(short Id, short AoClockType);
AioGetAoClockType = contecDll.AioGetAoClockType # long WINAPI AioGetAoClockType(short Id, short* AoClockType);
AioSetAoSamplingClock = contecDll.AioSetAoSamplingClock # long WINAPI AioSetAoSamplingClock(short Id, float AoSamplingClock);
AioGetAoSamplingClock = contecDll.AioGetAoSamplingClock # long WINAPI AioGetAoSamplingClock(short Id, float* AoSamplingClock);
AioSetAoClockEdge = contecDll.AioSetAoClockEdge # long WINAPI AioSetAoClockEdge(short Id, short AoClockEdge);
AioGetAoClockEdge = contecDll.AioGetAoClockEdge # long WINAPI AioGetAoClockEdge(short Id, short* AoClockEdge);
AioSetAoSamplingData = contecDll.AioSetAoSamplingData # long WINAPI AioSetAoSamplingData(short Id, long AoSamplingTimes, long* AoData);
AioSetAoSamplingDataEx = contecDll.AioSetAoSamplingDataEx # long WINAPI AioSetAoSamplingDataEx(short Id, long AoSamplingTimes, float* AoData);
AioGetAoSamplingTimes = contecDll.AioGetAoSamplingTimes # long WINAPI AioGetAoSamplingTimes(short Id, long* AoSamplingTimes);
AioSetAoStartTrigger = contecDll.AioSetAoStartTrigger # long WINAPI AioSetAoStartTrigger(short Id, short AoStartTrigger);
AioGetAoStartTrigger = contecDll.AioGetAoStartTrigger # long WINAPI AioGetAoStartTrigger(short Id, short* AoStartTrigger);
AioSetAoStopTrigger = contecDll.AioSetAoStopTrigger # long WINAPI AioSetAoStopTrigger(short Id, short AoStopTrigger);
AioGetAoStopTrigger = contecDll.AioGetAoStopTrigger # long WINAPI AioGetAoStopTrigger(short Id, short* AoStopTrigger);
AioSetAoEvent = contecDll.AioSetAoEvent # long WINAPI AioSetAoEvent(short Id, HWND hWnd, long AoEvent);
AioGetAoEvent = contecDll.AioGetAoEvent # long WINAPI AioGetAoEvent(short Id, HWND* hWnd, long* AoEvent);
AioSetAoEventSamplingTimes = contecDll.AioSetAoEventSamplingTimes # long WINAPI AioSetAoEventSamplingTimes(short Id, long AoSamplingTimes);
AioGetAoEventSamplingTimes = contecDll.AioGetAoEventSamplingTimes # long WINAPI AioGetAoEventSamplingTimes(short Id, long* AoSamplingTimes);
AioSetAoEventTransferTimes = contecDll.AioSetAoEventTransferTimes # long WINAPI AioSetAoEventTransferTimes(short Id, long AoTransferTimes);
AioGetAoEventTransferTimes = contecDll.AioGetAoEventTransferTimes # long WINAPI AioGetAoEventTransferTimes(short Id, long* AoTransferTimes);
AioStartAo = contecDll.AioStartAo # long WINAPI AioStartAo(short Id);
AioStopAo = contecDll.AioStopAo # long WINAPI AioStopAo(short Id);
AioEnableAo = contecDll.AioEnableAo # long WINAPI AioEnableAo(short Id, short AoChannel);
AioDisableAo = contecDll.AioDisableAo # long WINAPI AioDisableAo(short Id, short AoChannel);
AioGetAoStatus = contecDll.AioGetAoStatus # long WINAPI AioGetAoStatus(short Id, long* AoStatus);
AioGetAoSamplingCount = contecDll.AioGetAoSamplingCount # long WINAPI AioGetAoSamplingCount(short Id, long* AoSamplingCount);
AioGetAoTransferCount = contecDll.AioGetAoTransferCount # long WINAPI AioGetAoTransferCount(short Id, long* AoTransferCount);
AioGetAoTransferLap = contecDll.AioGetAoTransferLap # long WINAPI AioGetAoTransferLap(short Id, long* Lap);
AioGetAoRepeatCount = contecDll.AioGetAoRepeatCount # long WINAPI AioGetAoRepeatCount(short Id, long* AoRepeatCount);
AioResetAoStatus = contecDll.AioResetAoStatus # long WINAPI AioResetAoStatus(short Id);
AioResetAoMemory = contecDll.AioResetAoMemory # long WINAPI AioResetAoMemory(short Id);
AioSetDiFilter = contecDll.AioSetDiFilter # long WINAPI AioSetDiFilter(short Id, short Bit, float Value);
AioGetDiFilter = contecDll.AioGetDiFilter # long WINAPI AioGetDiFilter(short Id, short Bit, float* Value);
AioInputDiBit = contecDll.AioInputDiBit # long WINAPI AioInputDiBit(short Id, short DiBit, short* DiData);
AioOutputDoBit = contecDll.AioOutputDoBit # long WINAPI AioOutputDoBit(short Id, short DoBit, short DoData);
AioInputDiByte = contecDll.AioInputDiByte # long WINAPI AioInputDiByte(short Id, short DiPort, short* DiData);
AioOutputDoByte = contecDll.AioOutputDoByte # long WINAPI AioOutputDoByte(short Id, short DoPort, short DoData);
AioSetDioDirection = contecDll.AioSetDioDirection # long WINAPI AioSetDioDirection(short Id, long Dir);
AioGetCntMaxChannels = contecDll.AioGetCntMaxChannels # long WINAPI AioGetCntMaxChannels(short Id, short* CntMaxChannels);
AioSetCntComparisonMode = contecDll.AioSetCntComparisonMode # long WINAPI AioSetCntComparisonMode(short Id, short CntChannel, short CntMode);
AioGetCntComparisonMode = contecDll.AioGetCntComparisonMode # long WINAPI AioGetCntComparisonMode(short Id, short CntChannel, short* CntMode);
AioSetCntPresetReg = contecDll.AioSetCntPresetReg # long WINAPI AioSetCntPresetReg(short Id, short CntChannel, long PresetNumber, long* PresetData, short Flag);
AioSetCntComparisonReg = contecDll.AioSetCntComparisonReg # long WINAPI AioSetCntComparisonReg(short Id, short CntChannel, long ComparisonNumber, long* ComparisonData, short Flag);
AioSetCntInputSignal = contecDll.AioSetCntInputSignal # long WINAPI AioSetCntInputSignal(short Id, short CntChannel, short CntInputSignal);
AioGetCntInputSignal = contecDll.AioGetCntInputSignal # long WINAPI AioGetCntInputSignal(short Id, short CntChannel, short* CntInputSignal);
AioSetCntEvent = contecDll.AioSetCntEvent # long WINAPI AioSetCntEvent(short Id, short CntChannel, HWND hWnd, long CntEvent);
AioGetCntEvent = contecDll.AioGetCntEvent # long WINAPI AioGetCntEvent(short Id, short CntChannel, HWND* hWnd, long* CntEvent);
AioSetCntFilter = contecDll.AioSetCntFilter # long WINAPI AioSetCntFilter(short Id, short CntChannel, short Signal, float Value);
AioGetCntFilter = contecDll.AioGetCntFilter # long WINAPI AioGetCntFilter(short Id, short CntChannel, short Signal, float* Value);
AioStartCnt = contecDll.AioStartCnt # long WINAPI AioStartCnt(short Id, short CntChannel);
AioStopCnt = contecDll.AioStopCnt # long WINAPI AioStopCnt(short Id, short CntChannel);
AioPresetCnt = contecDll.AioPresetCnt # long WINAPI AioPresetCnt(short Id, short CntChannel, long PresetData);
AioGetCntStatus = contecDll.AioGetCntStatus # long WINAPI AioGetCntStatus(short Id, short CntChannel, long* CntStatus);
AioGetCntCount = contecDll.AioGetCntCount # long WINAPI AioGetCntCount(short Id, short CntChannel, long* Count);
AioResetCntStatus = contecDll.AioResetCntStatus # long WINAPI AioResetCntStatus(short Id, short CntChannel, long CntStatus);
AioSetTmEvent = contecDll.AioSetTmEvent # long WINAPI AioSetTmEvent(short Id, short TimerId, HWND hWnd, long TmEvent);
AioGetTmEvent = contecDll.AioGetTmEvent # long WINAPI AioGetTmEvent(short Id, short TimerId, HWND* hWnd, long* TmEvent);
AioStartTmTimer = contecDll.AioStartTmTimer # long WINAPI AioStartTmTimer(short Id, short TimerId, float Interval);
AioStopTmTimer = contecDll.AioStopTmTimer # long WINAPI AioStopTmTimer(short Id, short TimerId);
AioStartTmCount = contecDll.AioStartTmCount # long WINAPI AioStartTmCount(short Id, short TimerId);
AioStopTmCount = contecDll.AioStopTmCount # long WINAPI AioStopTmCount(short Id, short TimerId);
AioLapTmCount = contecDll.AioLapTmCount # long WINAPI AioLapTmCount(short Id, short TimerId, long* Lap);
AioResetTmCount = contecDll.AioResetTmCount # long WINAPI AioResetTmCount(short Id, short TimerId);
AioTmWait = contecDll.AioTmWait # long WINAPI AioTmWait(short Id, short TimerId, long Wait);
AioSetEcuSignal = contecDll.AioSetEcuSignal # long WINAPI AioSetEcuSignal(short Id, short Destination, short Source);
AioGetEcuSignal = contecDll.AioGetEcuSignal # long WINAPI AioGetEcuSignal(short Id, short Destination, short* Source);
AioGetCntmMaxChannels = contecDll.AioGetCntmMaxChannels # long WINAPI AioGetCntmMaxChannels(short Id, short* CntmMaxChannels);
AioSetCntmZMode = contecDll.AioSetCntmZMode # long WINAPI AioSetCntmZMode(short Id, short ChNo, short Mode);
AioSetCntmZLogic = contecDll.AioSetCntmZLogic # long WINAPI AioSetCntmZLogic(short Id, short ChNo, short ZLogic);
AioSelectCntmChannelSignal = contecDll.AioSelectCntmChannelSignal # long WINAPI AioSelectCntmChannelSignal(short Id, short ChNo, short SigType);
AioSetCntmCountDirection = contecDll.AioSetCntmCountDirection # long WINAPI AioSetCntmCountDirection(short Id, short ChNo, short Dir);
AioSetCntmOperationMode = contecDll.AioSetCntmOperationMode # long WINAPI AioSetCntmOperationMode(short Id, short ChNo, short Phase, short Mul, short SyncClr);
AioSetCntmDigitalFilter = contecDll.AioSetCntmDigitalFilter # long WINAPI AioSetCntmDigitalFilter(short Id, short ChNo, short FilterValue);
AioSetCntmPulseWidth = contecDll.AioSetCntmPulseWidth # long WINAPI AioSetCntmPulseWidth(short Id, short ChNo, short PlsWidth);
AioSetCntmOutputHardwareEvent = contecDll.AioSetCntmOutputHardwareEvent # long WINAPI AioSetCntmOutputHardwareEvent(short Id, short ChNo, short OutputLogic, unsignedlong EventType, short PulseWidth);
AioSetCntmInputHardwareEvent = contecDll.AioSetCntmInputHardwareEvent # long WINAPI AioSetCntmInputHardwareEvent(short Id, short ChNo, unsignedlong EventType, short RF0, short RF1, short Reserved);
AioSetCntmCountMatchHardwareEvent = contecDll.AioSetCntmCountMatchHardwareEvent # long WINAPI AioSetCntmCountMatchHardwareEvent(short Id, short ChNo, short RegisterNo, unsignedlong EventType, short Reserved);
AioSetCntmPresetRegister = contecDll.AioSetCntmPresetRegister # long WINAPI AioSetCntmPresetRegister(short Id, short ChNo, unsignedlong PresetData, short Reserved);
AioSetCntmTestPulse = contecDll.AioSetCntmTestPulse # long WINAPI AioSetCntmTestPulse(short Id, short CntmInternal, short CntmOut, short CntmReserved);
AioGetCntmZMode = contecDll.AioGetCntmZMode # long WINAPI AioGetCntmZMode(short Id, short ChNo, short* Mode);
AioGetCntmZLogic = contecDll.AioGetCntmZLogic # long WINAPI AioGetCntmZLogic(short Id, short ChNo, short* ZLogic);
AioGetCntmChannelSignal = contecDll.AioGetCntmChannelSignal # long WINAPI AioGetCntmChannelSignal(short Id, short CntmChNo, short* CntmSigType);
AioGetCntmCountDirection = contecDll.AioGetCntmCountDirection # long WINAPI AioGetCntmCountDirection(short Id, short ChNo, short* Dir);
AioGetCntmOperationMode = contecDll.AioGetCntmOperationMode # long WINAPI AioGetCntmOperationMode(short Id, short ChNo, short* Phase, short* Mul, short* SyncClr);
AioGetCntmDigitalFilter = contecDll.AioGetCntmDigitalFilter # long WINAPI AioGetCntmDigitalFilter(short Id, short ChNo, short* FilterValue);
AioGetCntmPulseWidth = contecDll.AioGetCntmPulseWidth # long WINAPI AioGetCntmPulseWidth(short Id, short ChNo, short* PlsWidth);
AioCntmStartCount = contecDll.AioCntmStartCount # long WINAPI AioCntmStartCount(short Id, short* ChNo, short ChNum);
AioCntmStopCount = contecDll.AioCntmStopCount # long WINAPI AioCntmStopCount(short Id, short* ChNo, short ChNum);
AioCntmPreset = contecDll.AioCntmPreset # long WINAPI AioCntmPreset(short Id, short* ChNo, short ChNum, unsignedlong* PresetData);
AioCntmZeroClearCount = contecDll.AioCntmZeroClearCount # long WINAPI AioCntmZeroClearCount(short Id, short* ChNo, short ChNum);
AioCntmReadCount = contecDll.AioCntmReadCount # long WINAPI AioCntmReadCount(short Id, short* ChNo, short ChNum, unsignedlong* CntDat);
AioCntmReadStatusEx = contecDll.AioCntmReadStatusEx # long WINAPI AioCntmReadStatusEx(short Id, short ChNo, unsignedlong* Sts);
AioCntmNotifyCountUp = contecDll.AioCntmNotifyCountUp # long WINAPI AioCntmNotifyCountUp(short Id, short ChNo, short RegNo, unsignedlong Count, HANDLE hWnd);
AioCntmStopNotifyCountUp = contecDll.AioCntmStopNotifyCountUp # long WINAPI AioCntmStopNotifyCountUp(short Id, short ChNo, short RegNo);
AioCntmCountUpCallbackProc = contecDll.AioCntmCountUpCallbackProc # long WINAPI AioCntmCountUpCallbackProc(short Id , void* CallBackProc , void* Param);
AioCntmNotifyCounterError = contecDll.AioCntmNotifyCounterError # long WINAPI AioCntmNotifyCounterError(short Id, HANDLE hWnd);
AioCntmStopNotifyCounterError = contecDll.AioCntmStopNotifyCounterError # long WINAPI AioCntmStopNotifyCounterError(short Id);
AioCntmCounterErrorCallbackProc = contecDll.AioCntmCounterErrorCallbackProc # long WINAPI AioCntmCounterErrorCallbackProc(short Id , void* CallBackProc , void* Param);
AioCntmNotifyCarryBorrow = contecDll.AioCntmNotifyCarryBorrow # long WINAPI AioCntmNotifyCarryBorrow(short Id, HANDLE hWnd);
AioCntmStopNotifyCarryBorrow = contecDll.AioCntmStopNotifyCarryBorrow # long WINAPI AioCntmStopNotifyCarryBorrow(short Id);
AioCntmCarryBorrowCallbackProc = contecDll.AioCntmCarryBorrowCallbackProc # long WINAPI AioCntmCarryBorrowCallbackProc(short Id, void* CallBackProc, void* Param);
AioCntmNotifyTimer = contecDll.AioCntmNotifyTimer # long WINAPI AioCntmNotifyTimer(short Id, unsignedlong TimeValue, HANDLE hWnd);
AioCntmStopNotifyTimer = contecDll.AioCntmStopNotifyTimer # long WINAPI AioCntmStopNotifyTimer(short Id);
AioCntmTimerCallbackProc = contecDll.AioCntmTimerCallbackProc # long WINAPI AioCntmTimerCallbackProc(short Id , void* CallBackProc , void* Param);
AioCntmInputDIByte = contecDll.AioCntmInputDIByte # long WINAPI AioCntmInputDIByte(short Id, short Reserved, BYTE* bData);
AioCntmOutputDOBit = contecDll.AioCntmOutputDOBit # long WINAPI AioCntmOutputDOBit(short Id, short AiomChNo, short Reserved, BYTE OutData);

# 未実装の関数---------------------------------------------------

# long WINAPI AioSetAiEvent(short Id, HWND hWnd, long AiEvent);
# long WINAPI AioGetAiEvent(short Id, HWND* hWnd, long* AiEvent);
# long WINAPI AioSetAiCallBackProc(short Id,long (_stdcall* pProc)(short Id, short AiEvent, WPARAM wParam, LPARAM lParam, void* Param), long AiEvent, void* Param);
# long WINAPI AioSetAoEvent(short Id, HWND hWnd, long AoEvent);
# long WINAPI AioGetAoEvent(short Id, HWND* hWnd, long* AoEvent);
# long WINAPI AioSetAoCallBackProc(short Id,long (_stdcall* pProc)(short Id, short AiEvent, WPARAM wParam, LPARAM lParam, void* Param), long AoEvent, void* Param);
# long WINAPI AioSetCntEvent(short Id, short CntChannel, HWND hWnd, long CntEvent);
# long WINAPI AioGetCntEvent(short Id, short CntChannel, HWND* hWnd, long* CntEvent);
# long WINAPI AioSetCntCallBackProc(short Id, short CntChannel,long (_stdcall* pProc)(short Id, short CntEvent, WPARAM wParam, LPARAM lParam, void* Param), long CntEvent, void* Param);
# long WINAPI AioSetTmEvent(short Id, short TimerId, HWND hWnd, long TmEvent);
# long WINAPI AioGetTmEvent(short Id, short TimerId, HWND* hWnd, long* TmEvent);
# long WINAPI AioSetTmCallBackProc(short Id, short TimerId,long (_stdcall* pProc)(short Id, short TmEvent, WPARAM wParam, LPARAM lParam, void* Param), long TmEvent, void* Param);
# long WINAPI AioCntmPreset(short Id, short* ChNo, short ChNum, unsignedlong* PresetData);
# long WINAPI AioCntmReadCount(short Id, short* ChNo, short ChNum, unsignedlong* CntDat);
# long WINAPI AioCntmReadStatusEx(short Id, short ChNo, unsignedlong* Sts);
# long WINAPI AioCntmNotifyCountUp(short Id, short ChNo, short RegNo, unsignedlong Count, HANDLE hWnd);
# long WINAPI AioCntmCountUpCallbackProc(short Id , void* CallBackProc , void* Param);
# long WINAPI AioCntmCountUpCallbackProc(short Id , void* CallBackProc , void* Param);
# long WINAPI AioCntmNotifyCounterError(short Id, HANDLE hWnd);
# long WINAPI AioCntmCounterErrorCallbackProc(short Id , void* CallBackProc , void* Param);
# long WINAPI AioCntmCounterErrorCallbackProc(short Id , void* CallBackProc , void* Param);
# long WINAPI AioCntmNotifyCarryBorrow(short Id, HANDLE hWnd);
# long WINAPI AioCntmCarryBorrowCallbackProc(short Id, void* CallBackProc, void* Param);
# long WINAPI AioCntmCarryBorrowCallbackProc(short Id, void* CallBackProc, void* Param);
# long WINAPI AioCntmNotifyTimer(short Id, unsignedlong TimeValue, HANDLE hWnd);
# long WINAPI AioCntmTimerCallbackProc(short Id , void* CallBackProc , void* Param);
# long WINAPI AioCntmTimerCallbackProc(short Id , void* CallBackProc , void* Param);
# long WINAPI AioCntmInputDIByte(short Id, short Reserved, BYTE* bData);
# long WINAPI AioCntmOutputDOBit(short Id, short AiomChNo, short Reserved, BYTE OutData);

# 以下はDllからExportされていない関数です-------------------------

# long WINAPI AioSetCntmDIType(short Id, short ChNo, short InputType);
# long WINAPI AioCntmReadStatus(short Id, short ChNo, short* Sts);


##
# @breif Contec API のラッパークラス
class Caio(object):
    """
    ## Contec API のラッパークラス
    """
    encode = "shift-jis"
    def __init__(self):
        self.ID = None
        pass


    # @breif
    # @param bytes DeviceName
    # @param int Id
    # @return int ret
    # @return bytes DeviceName
    # @return int Id
    def Init(self, DeviceName, Id):
        """
        ### breif
        プロト関数: AioInit
        ### args
        * _bytes_ DeviceName
        * _int_ Id
        \n### return
        * _int_ ret
        * _bytes_ DeviceName
        * _int_ Id
        """
        _DeviceName = c_char_p(DeviceName)
        _Id = c_short(Id)

        ret = AioInit(_DeviceName, byref(_Id))

        return ret, _DeviceName.value, _Id.value

    # @breif
    # @param int Id
    # @return int ret
    def Exit(self, Id):
        """
        ### breif
        プロト関数: AioExit
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioExit(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def ResetDevice(self, Id):
        """
        ### breif
        プロト関数: AioResetDevice
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioResetDevice(_Id)

        return ret

    # @breif
    # @param int ErrorCode
    # @param bytes ErrorString
    # @return int ret
    # @return bytes ErrorString
    def GetErrorString(self, ErrorCode, ErrorString):
        """
        ### breif
        プロト関数: AioGetErrorString
        ### args
        * _int_ ErrorCode
        * _bytes_ ErrorString
        \n### return
        * _int_ ret
        * _bytes_ ErrorString
        """
        _ErrorCode = c_long(ErrorCode)
        _ErrorString = c_char_p(ErrorString)

        ret = AioGetErrorString(_ErrorCode, _ErrorString)

        return ret, _ErrorString.value

    # @breif
    # @param int Index
    # @param bytes DeviceName
    # @param bytes Device
    # @return int ret
    # @return bytes DeviceName
    # @return bytes Device
    def QueryDeviceName(self, Index, DeviceName, Device):
        """
        ### breif
        プロト関数: AioQueryDeviceName
        ### args
        * _int_ Index
        * _bytes_ DeviceName
        * _bytes_ Device
        \n### return
        * _int_ ret
        * _bytes_ DeviceName
        * _bytes_ Device
        """
        _Index = c_short(Index)
        _DeviceName = c_char_p(DeviceName)
        _Device = c_char_p(Device)

        ret = AioQueryDeviceName(_Index, _DeviceName, _Device)

        return ret, _DeviceName.value, _Device.value

    # @breif
    # @param bytes Device
    # @param int DeviceType
    # @return int ret
    # @return bytes Device
    # @return int DeviceType
    def GetDeviceType(self, Device, DeviceType):
        """
        ### breif
        プロト関数: AioGetDeviceType
        ### args
        * _bytes_ Device
        * _int_ DeviceType
        \n### return
        * _int_ ret
        * _bytes_ Device
        * _int_ DeviceType
        """
        _Device = c_char_p(Device)
        _DeviceType = c_short(DeviceType)

        ret = AioGetDeviceType(_Device, byref(_DeviceType))

        return ret, _Device.value, _DeviceType.value

    # @breif
    # @param int Id
    # @param int Signal
    # @param float Value
    # @return int ret
    def SetControlFilter(self, Id, Signal, Value):
        """
        ### breif
        プロト関数: AioSetControlFilter
        ### args
        * _int_ Id
        * _int_ Signal
        * _float_ Value
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _Signal = c_short(Signal)
        _Value = c_float(Value)

        ret = AioSetControlFilter(_Id, _Signal, _Value)

        return ret

    # @breif
    # @param int Id
    # @param int Signal
    # @param float Value
    # @return int ret
    # @return float Value
    def GetControlFilter(self, Id, Signal, Value):
        """
        ### breif
        プロト関数: AioGetControlFilter
        ### args
        * _int_ Id
        * _int_ Signal
        * _float_ Value
        \n### return
        * _int_ ret
        * _float_ Value
        """
        _Id = c_short(Id)
        _Signal = c_short(Signal)
        _Value = c_float(Value)

        ret = AioGetControlFilter(_Id, _Signal, byref(_Value))

        return ret, _Value.value

    # @breif
    # @param int Id
    # @return int ret
    def ResetProcess(self, Id):
        """
        ### breif
        プロト関数: AioResetProcess
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioResetProcess(_Id)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int AiData
    # @return int ret
    # @return int AiData
    def SingleAi(self, Id, AiChannel, AiData):
        """
        ### breif
        プロト関数: AioSingleAi
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ AiData
        \n### return
        * _int_ ret
        * _int_ AiData
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiData = c_long(AiData)

        ret = AioSingleAi(_Id, _AiChannel, byref(_AiData))

        return ret, _AiData.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float AiData
    # @return int ret
    # @return float AiData
    def SingleAiEx(self, Id, AiChannel, AiData):
        """
        ### breif
        プロト関数: AioSingleAiEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ AiData
        \n### return
        * _int_ ret
        * _float_ AiData
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiData = c_float(AiData)

        ret = AioSingleAiEx(_Id, _AiChannel, byref(_AiData))

        return ret, _AiData.value

    # @breif
    # @param int Id
    # @param int AiChannels
    # @param int AiData
    # @return int ret
    # @return int AiData
    def MultiAi(self, Id, AiChannels, AiData):
        """
        ### breif
        プロト関数: AioMultiAi
        ### args
        * _int_ Id
        * _int_ AiChannels
        * _int_ AiData
        \n### return
        * _int_ ret
        * _int_ AiData
        """
        _Id = c_short(Id)
        _AiChannels = c_short(AiChannels)
        _AiData = c_long(AiData)

        ret = AioMultiAi(_Id, _AiChannels, byref(_AiData))

        return ret, _AiData.value

    # @breif
    # @param int Id
    # @param int AiChannels
    # @param float AiData
    # @return int ret
    # @return float AiData
    def MultiAiEx(self, Id, AiChannels, AiData):
        """
        ### breif
        プロト関数: AioMultiAiEx
        ### args
        * _int_ Id
        * _int_ AiChannels
        * _float_ AiData
        \n### return
        * _int_ ret
        * _float_ AiData
        """
        _Id = c_short(Id)
        _AiChannels = c_short(AiChannels)
        _AiData = c_float(AiData)

        ret = AioMultiAiEx(_Id, _AiChannels, byref(_AiData))

        return ret, _AiData.value

    # @breif
    # @param int Id
    # @param int AiResolution
    # @return int ret
    # @return int AiResolution
    def GetAiResolution(self, Id, AiResolution):
        """
        ### breif
        プロト関数: AioGetAiResolution
        ### args
        * _int_ Id
        * _int_ AiResolution
        \n### return
        * _int_ ret
        * _int_ AiResolution
        """
        _Id = c_short(Id)
        _AiResolution = c_short(AiResolution)

        ret = AioGetAiResolution(_Id, byref(_AiResolution))

        return ret, _AiResolution.value

    # @breif
    # @param int Id
    # @param int AiInputMethod
    # @return int ret
    def SetAiInputMethod(self, Id, AiInputMethod):
        """
        ### breif
        プロト関数: AioSetAiInputMethod
        ### args
        * _int_ Id
        * _int_ AiInputMethod
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiInputMethod = c_short(AiInputMethod)

        ret = AioSetAiInputMethod(_Id, _AiInputMethod)

        return ret

    # @breif
    # @param int Id
    # @param int AiInputMethod
    # @return int ret
    # @return int AiInputMethod
    def GetAiInputMethod(self, Id, AiInputMethod):
        """
        ### breif
        プロト関数: AioGetAiInputMethod
        ### args
        * _int_ Id
        * _int_ AiInputMethod
        \n### return
        * _int_ ret
        * _int_ AiInputMethod
        """
        _Id = c_short(Id)
        _AiInputMethod = c_short(AiInputMethod)

        ret = AioGetAiInputMethod(_Id, byref(_AiInputMethod))

        return ret, _AiInputMethod.value

    # @breif
    # @param int Id
    # @param int AiMaxChannels
    # @return int ret
    # @return int AiMaxChannels
    def GetAiMaxChannels(self, Id, AiMaxChannels):
        """
        ### breif
        プロト関数: AioGetAiMaxChannels
        ### args
        * _int_ Id
        * _int_ AiMaxChannels
        \n### return
        * _int_ ret
        * _int_ AiMaxChannels
        """
        _Id = c_short(Id)
        _AiMaxChannels = c_short(AiMaxChannels)

        ret = AioGetAiMaxChannels(_Id, byref(_AiMaxChannels))

        return ret, _AiMaxChannels.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Enabled
    # @return int ret
    def SetAiChannel(self, Id, AiChannel, Enabled):
        """
        ### breif
        プロト関数: AioSetAiChannel
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Enabled
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Enabled = c_short(Enabled)

        ret = AioSetAiChannel(_Id, _AiChannel, _Enabled)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Enabled
    # @return int ret
    # @return int Enabled
    def GetAiChannel(self, Id, AiChannel, Enabled):
        """
        ### breif
        プロト関数: AioGetAiChannel
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Enabled
        \n### return
        * _int_ ret
        * _int_ Enabled
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Enabled = c_short(Enabled)

        ret = AioGetAiChannel(_Id, _AiChannel, byref(_Enabled))

        return ret, _Enabled.value

    # @breif
    # @param int Id
    # @param int AiChannels
    # @return int ret
    def SetAiChannels(self, Id, AiChannels):
        """
        ### breif
        プロト関数: AioSetAiChannels
        ### args
        * _int_ Id
        * _int_ AiChannels
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannels = c_short(AiChannels)

        ret = AioSetAiChannels(_Id, _AiChannels)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannels
    # @return int ret
    # @return int AiChannels
    def GetAiChannels(self, Id, AiChannels):
        """
        ### breif
        プロト関数: AioGetAiChannels
        ### args
        * _int_ Id
        * _int_ AiChannels
        \n### return
        * _int_ ret
        * _int_ AiChannels
        """
        _Id = c_short(Id)
        _AiChannels = c_short(AiChannels)

        ret = AioGetAiChannels(_Id, byref(_AiChannels))

        return ret, _AiChannels.value

    # @breif
    # @param int Id
    # @param int AiSequence
    # @param int AiChannel
    # @return int ret
    def SetAiChannelSequence(self, Id, AiSequence, AiChannel):
        """
        ### breif
        プロト関数: AioSetAiChannelSequence
        ### args
        * _int_ Id
        * _int_ AiSequence
        * _int_ AiChannel
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiSequence = c_short(AiSequence)
        _AiChannel = c_short(AiChannel)

        ret = AioSetAiChannelSequence(_Id, _AiSequence, _AiChannel)

        return ret

    # @breif
    # @param int Id
    # @param int AiSequence
    # @param int AiChannel
    # @return int ret
    # @return int AiChannel
    def GetAiChannelSequence(self, Id, AiSequence, AiChannel):
        """
        ### breif
        プロト関数: AioGetAiChannelSequence
        ### args
        * _int_ Id
        * _int_ AiSequence
        * _int_ AiChannel
        \n### return
        * _int_ ret
        * _int_ AiChannel
        """
        _Id = c_short(Id)
        _AiSequence = c_short(AiSequence)
        _AiChannel = c_short(AiChannel)

        ret = AioGetAiChannelSequence(_Id, _AiSequence, byref(_AiChannel))

        return ret, _AiChannel.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int AiRange
    # @return int ret
    def SetAiRange(self, Id, AiChannel, AiRange):
        """
        ### breif
        プロト関数: AioSetAiRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ AiRange
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiRange = c_short(AiRange)

        ret = AioSetAiRange(_Id, _AiChannel, _AiRange)

        return ret

    # @breif
    # @param int Id
    # @param int AiRange
    # @return int ret
    def SetAiRangeAll(self, Id, AiRange):
        """
        ### breif
        プロト関数: AioSetAiRangeAll
        ### args
        * _int_ Id
        * _int_ AiRange
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiRange = c_short(AiRange)

        ret = AioSetAiRangeAll(_Id, _AiRange)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int AiRange
    # @return int ret
    # @return int AiRange
    def GetAiRange(self, Id, AiChannel, AiRange):
        """
        ### breif
        プロト関数: AioGetAiRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ AiRange
        \n### return
        * _int_ ret
        * _int_ AiRange
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiRange = c_short(AiRange)

        ret = AioGetAiRange(_Id, _AiChannel, byref(_AiRange))

        return ret, _AiRange.value

    # @breif
    # @param int Id
    # @param int AiTransferMode
    # @return int ret
    def SetAiTransferMode(self, Id, AiTransferMode):
        """
        ### breif
        プロト関数: AioSetAiTransferMode
        ### args
        * _int_ Id
        * _int_ AiTransferMode
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiTransferMode = c_short(AiTransferMode)

        ret = AioSetAiTransferMode(_Id, _AiTransferMode)

        return ret

    # @breif
    # @param int Id
    # @param int AiTransferMode
    # @return int ret
    # @return int AiTransferMode
    def GetAiTransferMode(self, Id, AiTransferMode):
        """
        ### breif
        プロト関数: AioGetAiTransferMode
        ### args
        * _int_ Id
        * _int_ AiTransferMode
        \n### return
        * _int_ ret
        * _int_ AiTransferMode
        """
        _Id = c_short(Id)
        _AiTransferMode = c_short(AiTransferMode)

        ret = AioGetAiTransferMode(_Id, byref(_AiTransferMode))

        return ret, _AiTransferMode.value

    # @breif
    # @param int Id
    # @param int AiDeviceBufferMode
    # @return int ret
    def SetAiDeviceBufferMode(self, Id, AiDeviceBufferMode):
        """
        ### breif
        プロト関数: AioSetAiDeviceBufferMode
        ### args
        * _int_ Id
        * _int_ AiDeviceBufferMode
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiDeviceBufferMode = c_short(AiDeviceBufferMode)

        ret = AioSetAiDeviceBufferMode(_Id, _AiDeviceBufferMode)

        return ret

    # @breif
    # @param int Id
    # @param int AiDeviceBufferMode
    # @return int ret
    # @return int AiDeviceBufferMode
    def GetAiDeviceBufferMode(self, Id, AiDeviceBufferMode):
        """
        ### breif
        プロト関数: AioGetAiDeviceBufferMode
        ### args
        * _int_ Id
        * _int_ AiDeviceBufferMode
        \n### return
        * _int_ ret
        * _int_ AiDeviceBufferMode
        """
        _Id = c_short(Id)
        _AiDeviceBufferMode = c_short(AiDeviceBufferMode)

        ret = AioGetAiDeviceBufferMode(_Id, byref(_AiDeviceBufferMode))

        return ret, _AiDeviceBufferMode.value

    # @breif
    # @param int Id
    # @param int AiMemorySize
    # @return int ret
    def SetAiMemorySize(self, Id, AiMemorySize):
        """
        ### breif
        プロト関数: AioSetAiMemorySize
        ### args
        * _int_ Id
        * _int_ AiMemorySize
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiMemorySize = c_long(AiMemorySize)

        ret = AioSetAiMemorySize(_Id, _AiMemorySize)

        return ret

    # @breif
    # @param int Id
    # @param int AiMemorySize
    # @return int ret
    # @return int AiMemorySize
    def GetAiMemorySize(self, Id, AiMemorySize):
        """
        ### breif
        プロト関数: AioGetAiMemorySize
        ### args
        * _int_ Id
        * _int_ AiMemorySize
        \n### return
        * _int_ ret
        * _int_ AiMemorySize
        """
        _Id = c_short(Id)
        _AiMemorySize = c_long(AiMemorySize)

        ret = AioGetAiMemorySize(_Id, byref(_AiMemorySize))

        return ret, _AiMemorySize.value

    # @breif
    # @param int Id
    # @param int DataNumber
    # @param int Buffer
    # @return int ret
    # @return int Buffer
    def SetAiTransferData(self, Id, DataNumber, Buffer):
        """
        ### breif
        プロト関数: AioSetAiTransferData
        ### args
        * _int_ Id
        * _int_ DataNumber
        * _int_ Buffer
        \n### return
        * _int_ ret
        * _int_ Buffer
        """
        _Id = c_short(Id)
        _DataNumber = c_long(DataNumber)
        _Buffer = c_long(Buffer)

        ret = AioSetAiTransferData(_Id, _DataNumber, byref(_Buffer))

        return ret, _Buffer.value

    # @breif
    # @param int Id
    # @param int AttachedData
    # @return int ret
    def SetAiAttachedData(self, Id, AttachedData):
        """
        ### breif
        プロト関数: AioSetAiAttachedData
        ### args
        * _int_ Id
        * _int_ AttachedData
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AttachedData = c_long(AttachedData)

        ret = AioSetAiAttachedData(_Id, _AttachedData)

        return ret

    # @breif
    # @param int Id
    # @param int DataSize
    # @return int ret
    # @return int DataSize
    def GetAiSamplingDataSize(self, Id, DataSize):
        """
        ### breif
        プロト関数: AioGetAiSamplingDataSize
        ### args
        * _int_ Id
        * _int_ DataSize
        \n### return
        * _int_ ret
        * _int_ DataSize
        """
        _Id = c_short(Id)
        _DataSize = c_short(DataSize)

        ret = AioGetAiSamplingDataSize(_Id, byref(_DataSize))

        return ret, _DataSize.value

    # @breif
    # @param int Id
    # @param int AiMemoryType
    # @return int ret
    def SetAiMemoryType(self, Id, AiMemoryType):
        """
        ### breif
        プロト関数: AioSetAiMemoryType
        ### args
        * _int_ Id
        * _int_ AiMemoryType
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiMemoryType = c_short(AiMemoryType)

        ret = AioSetAiMemoryType(_Id, _AiMemoryType)

        return ret

    # @breif
    # @param int Id
    # @param int AiMemoryType
    # @return int ret
    # @return int AiMemoryType
    def GetAiMemoryType(self, Id, AiMemoryType):
        """
        ### breif
        プロト関数: AioGetAiMemoryType
        ### args
        * _int_ Id
        * _int_ AiMemoryType
        \n### return
        * _int_ ret
        * _int_ AiMemoryType
        """
        _Id = c_short(Id)
        _AiMemoryType = c_short(AiMemoryType)

        ret = AioGetAiMemoryType(_Id, byref(_AiMemoryType))

        return ret, _AiMemoryType.value

    # @breif
    # @param int Id
    # @param int AiRepeatTimes
    # @return int ret
    def SetAiRepeatTimes(self, Id, AiRepeatTimes):
        """
        ### breif
        プロト関数: AioSetAiRepeatTimes
        ### args
        * _int_ Id
        * _int_ AiRepeatTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiRepeatTimes = c_long(AiRepeatTimes)

        ret = AioSetAiRepeatTimes(_Id, _AiRepeatTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiRepeatTimes
    # @return int ret
    # @return int AiRepeatTimes
    def GetAiRepeatTimes(self, Id, AiRepeatTimes):
        """
        ### breif
        プロト関数: AioGetAiRepeatTimes
        ### args
        * _int_ Id
        * _int_ AiRepeatTimes
        \n### return
        * _int_ ret
        * _int_ AiRepeatTimes
        """
        _Id = c_short(Id)
        _AiRepeatTimes = c_long(AiRepeatTimes)

        ret = AioGetAiRepeatTimes(_Id, byref(_AiRepeatTimes))

        return ret, _AiRepeatTimes.value

    # @breif
    # @param int Id
    # @param int AiClockType
    # @return int ret
    def SetAiClockType(self, Id, AiClockType):
        """
        ### breif
        プロト関数: AioSetAiClockType
        ### args
        * _int_ Id
        * _int_ AiClockType
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiClockType = c_short(AiClockType)

        ret = AioSetAiClockType(_Id, _AiClockType)

        return ret

    # @breif
    # @param int Id
    # @param int AiClockType
    # @return int ret
    # @return int AiClockType
    def GetAiClockType(self, Id, AiClockType):
        """
        ### breif
        プロト関数: AioGetAiClockType
        ### args
        * _int_ Id
        * _int_ AiClockType
        \n### return
        * _int_ ret
        * _int_ AiClockType
        """
        _Id = c_short(Id)
        _AiClockType = c_short(AiClockType)

        ret = AioGetAiClockType(_Id, byref(_AiClockType))

        return ret, _AiClockType.value

    # @breif
    # @param int Id
    # @param float AiSamplingClock
    # @return int ret
    def SetAiSamplingClock(self, Id, AiSamplingClock):
        """
        ### breif
        プロト関数: AioSetAiSamplingClock
        ### args
        * _int_ Id
        * _float_ AiSamplingClock
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiSamplingClock = c_float(AiSamplingClock)

        ret = AioSetAiSamplingClock(_Id, _AiSamplingClock)

        return ret

    # @breif
    # @param int Id
    # @param float AiSamplingClock
    # @return int ret
    # @return float AiSamplingClock
    def GetAiSamplingClock(self, Id, AiSamplingClock):
        """
        ### breif
        プロト関数: AioGetAiSamplingClock
        ### args
        * _int_ Id
        * _float_ AiSamplingClock
        \n### return
        * _int_ ret
        * _float_ AiSamplingClock
        """
        _Id = c_short(Id)
        _AiSamplingClock = c_float(AiSamplingClock)

        ret = AioGetAiSamplingClock(_Id, byref(_AiSamplingClock))

        return ret, _AiSamplingClock.value

    # @breif
    # @param int Id
    # @param float AiScanClock
    # @return int ret
    def SetAiScanClock(self, Id, AiScanClock):
        """
        ### breif
        プロト関数: AioSetAiScanClock
        ### args
        * _int_ Id
        * _float_ AiScanClock
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiScanClock = c_float(AiScanClock)

        ret = AioSetAiScanClock(_Id, _AiScanClock)

        return ret

    # @breif
    # @param int Id
    # @param float AiScanClock
    # @return int ret
    # @return float AiScanClock
    def GetAiScanClock(self, Id, AiScanClock):
        """
        ### breif
        プロト関数: AioGetAiScanClock
        ### args
        * _int_ Id
        * _float_ AiScanClock
        \n### return
        * _int_ ret
        * _float_ AiScanClock
        """
        _Id = c_short(Id)
        _AiScanClock = c_float(AiScanClock)

        ret = AioGetAiScanClock(_Id, byref(_AiScanClock))

        return ret, _AiScanClock.value

    # @breif
    # @param int Id
    # @param int AoClockEdge
    # @return int ret
    def SetAiClockEdge(self, Id, AoClockEdge):
        """
        ### breif
        プロト関数: AioSetAiClockEdge
        ### args
        * _int_ Id
        * _int_ AoClockEdge
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoClockEdge = c_short(AoClockEdge)

        ret = AioSetAiClockEdge(_Id, _AoClockEdge)

        return ret

    # @breif
    # @param int Id
    # @param int AoClockEdge
    # @return int ret
    # @return int AoClockEdge
    def GetAiClockEdge(self, Id, AoClockEdge):
        """
        ### breif
        プロト関数: AioGetAiClockEdge
        ### args
        * _int_ Id
        * _int_ AoClockEdge
        \n### return
        * _int_ ret
        * _int_ AoClockEdge
        """
        _Id = c_short(Id)
        _AoClockEdge = c_short(AoClockEdge)

        ret = AioGetAiClockEdge(_Id, byref(_AoClockEdge))

        return ret, _AoClockEdge.value

    # @breif
    # @param int Id
    # @param int AiStartTrigger
    # @return int ret
    def SetAiStartTrigger(self, Id, AiStartTrigger):
        """
        ### breif
        プロト関数: AioSetAiStartTrigger
        ### args
        * _int_ Id
        * _int_ AiStartTrigger
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiStartTrigger = c_short(AiStartTrigger)

        ret = AioSetAiStartTrigger(_Id, _AiStartTrigger)

        return ret

    # @breif
    # @param int Id
    # @param int AiStartTrigger
    # @return int ret
    # @return int AiStartTrigger
    def GetAiStartTrigger(self, Id, AiStartTrigger):
        """
        ### breif
        プロト関数: AioGetAiStartTrigger
        ### args
        * _int_ Id
        * _int_ AiStartTrigger
        \n### return
        * _int_ ret
        * _int_ AiStartTrigger
        """
        _Id = c_short(Id)
        _AiStartTrigger = c_short(AiStartTrigger)

        ret = AioGetAiStartTrigger(_Id, byref(_AiStartTrigger))

        return ret, _AiStartTrigger.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int AiStartLevel
    # @param int AiDirection
    # @return int ret
    def SetAiStartLevel(self, Id, AiChannel, AiStartLevel, AiDirection):
        """
        ### breif
        プロト関数: AioSetAiStartLevel
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ AiStartLevel
        * _int_ AiDirection
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiStartLevel = c_long(AiStartLevel)
        _AiDirection = c_short(AiDirection)

        ret = AioSetAiStartLevel(_Id, _AiChannel, _AiStartLevel, _AiDirection)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float AiStartLevel
    # @param int AiDirection
    # @return int ret
    def SetAiStartLevelEx(self, Id, AiChannel, AiStartLevel, AiDirection):
        """
        ### breif
        プロト関数: AioSetAiStartLevelEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ AiStartLevel
        * _int_ AiDirection
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiStartLevel = c_float(AiStartLevel)
        _AiDirection = c_short(AiDirection)

        ret = AioSetAiStartLevelEx(_Id, _AiChannel, _AiStartLevel, _AiDirection)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int AiStartLevel
    # @param int AiDirection
    # @return int ret
    # @return int AiStartLevel
    # @return int AiDirection
    def GetAiStartLevel(self, Id, AiChannel, AiStartLevel, AiDirection):
        """
        ### breif
        プロト関数: AioGetAiStartLevel
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ AiStartLevel
        * _int_ AiDirection
        \n### return
        * _int_ ret
        * _int_ AiStartLevel
        * _int_ AiDirection
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiStartLevel = c_long(AiStartLevel)
        _AiDirection = c_short(AiDirection)

        ret = AioGetAiStartLevel(_Id, _AiChannel, byref(_AiStartLevel), byref(_AiDirection))

        return ret, _AiStartLevel.value, _AiDirection.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float AiStartLevel
    # @param int AiDirection
    # @return int ret
    # @return float AiStartLevel
    # @return int AiDirection
    def GetAiStartLevelEx(self, Id, AiChannel, AiStartLevel, AiDirection):
        """
        ### breif
        プロト関数: AioGetAiStartLevelEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ AiStartLevel
        * _int_ AiDirection
        \n### return
        * _int_ ret
        * _float_ AiStartLevel
        * _int_ AiDirection
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiStartLevel = c_float(AiStartLevel)
        _AiDirection = c_short(AiDirection)

        ret = AioGetAiStartLevelEx(_Id, _AiChannel, byref(_AiStartLevel), byref(_AiDirection))

        return ret, _AiStartLevel.value, _AiDirection.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Level1
    # @param int Level2
    # @param int StateTimes
    # @return int ret
    def SetAiStartInRange(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioSetAiStartInRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_long(Level1)
        _Level2 = c_long(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioSetAiStartInRange(_Id, _AiChannel, _Level1, _Level2, _StateTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float Level1
    # @param float Level2
    # @param int StateTimes
    # @return int ret
    def SetAiStartInRangeEx(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioSetAiStartInRangeEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_float(Level1)
        _Level2 = c_float(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioSetAiStartInRangeEx(_Id, _AiChannel, _Level1, _Level2, _StateTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Level1
    # @param int Level2
    # @param int StateTimes
    # @return int ret
    # @return int Level1
    # @return int Level2
    # @return int StateTimes
    def GetAiStartInRange(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioGetAiStartInRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_long(Level1)
        _Level2 = c_long(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioGetAiStartInRange(_Id, _AiChannel, byref(_Level1), byref(_Level2), byref(_StateTimes))

        return ret, _Level1.value, _Level2.value, _StateTimes.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float Level1
    # @param float Level2
    # @param int StateTimes
    # @return int ret
    # @return float Level1
    # @return float Level2
    # @return int StateTimes
    def GetAiStartInRangeEx(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioGetAiStartInRangeEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_float(Level1)
        _Level2 = c_float(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioGetAiStartInRangeEx(_Id, _AiChannel, byref(_Level1), byref(_Level2), byref(_StateTimes))

        return ret, _Level1.value, _Level2.value, _StateTimes.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Level1
    # @param int Level2
    # @param int StateTimes
    # @return int ret
    def SetAiStartOutRange(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioSetAiStartOutRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_long(Level1)
        _Level2 = c_long(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioSetAiStartOutRange(_Id, _AiChannel, _Level1, _Level2, _StateTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float Level1
    # @param float Level2
    # @param int StateTimes
    # @return int ret
    def SetAiStartOutRangeEx(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioSetAiStartOutRangeEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_float(Level1)
        _Level2 = c_float(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioSetAiStartOutRangeEx(_Id, _AiChannel, _Level1, _Level2, _StateTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Level1
    # @param int Level2
    # @param int StateTimes
    # @return int ret
    # @return int Level1
    # @return int Level2
    # @return int StateTimes
    def GetAiStartOutRange(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioGetAiStartOutRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_long(Level1)
        _Level2 = c_long(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioGetAiStartOutRange(_Id, _AiChannel, byref(_Level1), byref(_Level2), byref(_StateTimes))

        return ret, _Level1.value, _Level2.value, _StateTimes.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float Level1
    # @param float Level2
    # @param int StateTimes
    # @return int ret
    # @return float Level1
    # @return float Level2
    # @return int StateTimes
    def GetAiStartOutRangeEx(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioGetAiStartOutRangeEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_float(Level1)
        _Level2 = c_float(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioGetAiStartOutRangeEx(_Id, _AiChannel, byref(_Level1), byref(_Level2), byref(_StateTimes))

        return ret, _Level1.value, _Level2.value, _StateTimes.value

    # @breif
    # @param int Id
    # @param int AiStopTrigger
    # @return int ret
    def SetAiStopTrigger(self, Id, AiStopTrigger):
        """
        ### breif
        プロト関数: AioSetAiStopTrigger
        ### args
        * _int_ Id
        * _int_ AiStopTrigger
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiStopTrigger = c_short(AiStopTrigger)

        ret = AioSetAiStopTrigger(_Id, _AiStopTrigger)

        return ret

    # @breif
    # @param int Id
    # @param int AiStopTrigger
    # @return int ret
    # @return int AiStopTrigger
    def GetAiStopTrigger(self, Id, AiStopTrigger):
        """
        ### breif
        プロト関数: AioGetAiStopTrigger
        ### args
        * _int_ Id
        * _int_ AiStopTrigger
        \n### return
        * _int_ ret
        * _int_ AiStopTrigger
        """
        _Id = c_short(Id)
        _AiStopTrigger = c_short(AiStopTrigger)

        ret = AioGetAiStopTrigger(_Id, byref(_AiStopTrigger))

        return ret, _AiStopTrigger.value

    # @breif
    # @param int Id
    # @param int AiStopTimes
    # @return int ret
    def SetAiStopTimes(self, Id, AiStopTimes):
        """
        ### breif
        プロト関数: AioSetAiStopTimes
        ### args
        * _int_ Id
        * _int_ AiStopTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiStopTimes = c_long(AiStopTimes)

        ret = AioSetAiStopTimes(_Id, _AiStopTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiStopTimes
    # @return int ret
    # @return int AiStopTimes
    def GetAiStopTimes(self, Id, AiStopTimes):
        """
        ### breif
        プロト関数: AioGetAiStopTimes
        ### args
        * _int_ Id
        * _int_ AiStopTimes
        \n### return
        * _int_ ret
        * _int_ AiStopTimes
        """
        _Id = c_short(Id)
        _AiStopTimes = c_long(AiStopTimes)

        ret = AioGetAiStopTimes(_Id, byref(_AiStopTimes))

        return ret, _AiStopTimes.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int AiStopLevel
    # @param int AiDirection
    # @return int ret
    def SetAiStopLevel(self, Id, AiChannel, AiStopLevel, AiDirection):
        """
        ### breif
        プロト関数: AioSetAiStopLevel
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ AiStopLevel
        * _int_ AiDirection
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiStopLevel = c_long(AiStopLevel)
        _AiDirection = c_short(AiDirection)

        ret = AioSetAiStopLevel(_Id, _AiChannel, _AiStopLevel, _AiDirection)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float AiStopLevel
    # @param int AiDirection
    # @return int ret
    def SetAiStopLevelEx(self, Id, AiChannel, AiStopLevel, AiDirection):
        """
        ### breif
        プロト関数: AioSetAiStopLevelEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ AiStopLevel
        * _int_ AiDirection
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiStopLevel = c_float(AiStopLevel)
        _AiDirection = c_short(AiDirection)

        ret = AioSetAiStopLevelEx(_Id, _AiChannel, _AiStopLevel, _AiDirection)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int AiStopLevel
    # @param int AiDirection
    # @return int ret
    # @return int AiStopLevel
    # @return int AiDirection
    def GetAiStopLevel(self, Id, AiChannel, AiStopLevel, AiDirection):
        """
        ### breif
        プロト関数: AioGetAiStopLevel
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ AiStopLevel
        * _int_ AiDirection
        \n### return
        * _int_ ret
        * _int_ AiStopLevel
        * _int_ AiDirection
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiStopLevel = c_long(AiStopLevel)
        _AiDirection = c_short(AiDirection)

        ret = AioGetAiStopLevel(_Id, _AiChannel, byref(_AiStopLevel), byref(_AiDirection))

        return ret, _AiStopLevel.value, _AiDirection.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float AiStopLevel
    # @param int AiDirection
    # @return int ret
    # @return float AiStopLevel
    # @return int AiDirection
    def GetAiStopLevelEx(self, Id, AiChannel, AiStopLevel, AiDirection):
        """
        ### breif
        プロト関数: AioGetAiStopLevelEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ AiStopLevel
        * _int_ AiDirection
        \n### return
        * _int_ ret
        * _float_ AiStopLevel
        * _int_ AiDirection
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _AiStopLevel = c_float(AiStopLevel)
        _AiDirection = c_short(AiDirection)

        ret = AioGetAiStopLevelEx(_Id, _AiChannel, byref(_AiStopLevel), byref(_AiDirection))

        return ret, _AiStopLevel.value, _AiDirection.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Level1
    # @param int Level2
    # @param int StateTimes
    # @return int ret
    def SetAiStopInRange(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioSetAiStopInRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_long(Level1)
        _Level2 = c_long(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioSetAiStopInRange(_Id, _AiChannel, _Level1, _Level2, _StateTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float Level1
    # @param float Level2
    # @param int StateTimes
    # @return int ret
    def SetAiStopInRangeEx(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioSetAiStopInRangeEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_float(Level1)
        _Level2 = c_float(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioSetAiStopInRangeEx(_Id, _AiChannel, _Level1, _Level2, _StateTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Level1
    # @param int Level2
    # @param int StateTimes
    # @return int ret
    # @return int Level1
    # @return int Level2
    # @return int StateTimes
    def GetAiStopInRange(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioGetAiStopInRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_long(Level1)
        _Level2 = c_long(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioGetAiStopInRange(_Id, _AiChannel, byref(_Level1), byref(_Level2), byref(_StateTimes))

        return ret, _Level1.value, _Level2.value, _StateTimes.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float Level1
    # @param float Level2
    # @param int StateTimes
    # @return int ret
    # @return float Level1
    # @return float Level2
    # @return int StateTimes
    def GetAiStopInRangeEx(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioGetAiStopInRangeEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_float(Level1)
        _Level2 = c_float(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioGetAiStopInRangeEx(_Id, _AiChannel, byref(_Level1), byref(_Level2), byref(_StateTimes))

        return ret, _Level1.value, _Level2.value, _StateTimes.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Level1
    # @param int Level2
    # @param int StateTimes
    # @return int ret
    def SetAiStopOutRange(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioSetAiStopOutRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_long(Level1)
        _Level2 = c_long(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioSetAiStopOutRange(_Id, _AiChannel, _Level1, _Level2, _StateTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float Level1
    # @param float Level2
    # @param int StateTimes
    # @return int ret
    def SetAiStopOutRangeEx(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioSetAiStopOutRangeEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_float(Level1)
        _Level2 = c_float(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioSetAiStopOutRangeEx(_Id, _AiChannel, _Level1, _Level2, _StateTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param int Level1
    # @param int Level2
    # @param int StateTimes
    # @return int ret
    # @return int Level1
    # @return int Level2
    # @return int StateTimes
    def GetAiStopOutRange(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioGetAiStopOutRange
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        * _int_ Level1
        * _int_ Level2
        * _int_ StateTimes
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_long(Level1)
        _Level2 = c_long(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioGetAiStopOutRange(_Id, _AiChannel, byref(_Level1), byref(_Level2), byref(_StateTimes))

        return ret, _Level1.value, _Level2.value, _StateTimes.value

    # @breif
    # @param int Id
    # @param int AiChannel
    # @param float Level1
    # @param float Level2
    # @param int StateTimes
    # @return int ret
    # @return float Level1
    # @return float Level2
    # @return int StateTimes
    def GetAiStopOutRangeEx(self, Id, AiChannel, Level1, Level2, StateTimes):
        """
        ### breif
        プロト関数: AioGetAiStopOutRangeEx
        ### args
        * _int_ Id
        * _int_ AiChannel
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        \n### return
        * _int_ ret
        * _float_ Level1
        * _float_ Level2
        * _int_ StateTimes
        """
        _Id = c_short(Id)
        _AiChannel = c_short(AiChannel)
        _Level1 = c_float(Level1)
        _Level2 = c_float(Level2)
        _StateTimes = c_long(StateTimes)

        ret = AioGetAiStopOutRangeEx(_Id, _AiChannel, byref(_Level1), byref(_Level2), byref(_StateTimes))

        return ret, _Level1.value, _Level2.value, _StateTimes.value

    # @breif
    # @param int Id
    # @param int AiStopDelayTimes
    # @return int ret
    def SetAiStopDelayTimes(self, Id, AiStopDelayTimes):
        """
        ### breif
        プロト関数: AioSetAiStopDelayTimes
        ### args
        * _int_ Id
        * _int_ AiStopDelayTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiStopDelayTimes = c_long(AiStopDelayTimes)

        ret = AioSetAiStopDelayTimes(_Id, _AiStopDelayTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiStopDelayTimes
    # @return int ret
    # @return int AiStopDelayTimes
    def GetAiStopDelayTimes(self, Id, AiStopDelayTimes):
        """
        ### breif
        プロト関数: AioGetAiStopDelayTimes
        ### args
        * _int_ Id
        * _int_ AiStopDelayTimes
        \n### return
        * _int_ ret
        * _int_ AiStopDelayTimes
        """
        _Id = c_short(Id)
        _AiStopDelayTimes = c_long(AiStopDelayTimes)

        ret = AioGetAiStopDelayTimes(_Id, byref(_AiStopDelayTimes))

        return ret, _AiStopDelayTimes.value

    # @breif
    # @param int Id
    # @param int AiEvent
    # @return int ret
    def SetAiEvent(self, Id, AiEvent):
        """
        ### breif
        プロト関数: AioSetAiEvent
        ### args
        * _int_ Id
        * _int_ AiEvent
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiEvent = c_long(AiEvent)

        ret = AioSetAiEvent(_Id, _AiEvent)

        return ret

    # @breif
    # @param int Id
    # @param int AiEvent
    # @return int ret
    # @return int AiEvent
    def GetAiEvent(self, Id, AiEvent):
        """
        ### breif
        プロト関数: AioGetAiEvent
        ### args
        * _int_ Id
        * _int_ AiEvent
        \n### return
        * _int_ ret
        * _int_ AiEvent
        """
        _Id = c_short(Id)
        _AiEvent = c_long(AiEvent)

        ret = AioGetAiEvent(_Id, byref(_AiEvent))

        return ret, _AiEvent.value

    # @breif
    # @param int Id
    # @param int AiSamplingTimes
    # @return int ret
    def SetAiEventSamplingTimes(self, Id, AiSamplingTimes):
        """
        ### breif
        プロト関数: AioSetAiEventSamplingTimes
        ### args
        * _int_ Id
        * _int_ AiSamplingTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiSamplingTimes = c_long(AiSamplingTimes)

        ret = AioSetAiEventSamplingTimes(_Id, _AiSamplingTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiSamplingTimes
    # @return int ret
    # @return int AiSamplingTimes
    def GetAiEventSamplingTimes(self, Id, AiSamplingTimes):
        """
        ### breif
        プロト関数: AioGetAiEventSamplingTimes
        ### args
        * _int_ Id
        * _int_ AiSamplingTimes
        \n### return
        * _int_ ret
        * _int_ AiSamplingTimes
        """
        _Id = c_short(Id)
        _AiSamplingTimes = c_long(AiSamplingTimes)

        ret = AioGetAiEventSamplingTimes(_Id, byref(_AiSamplingTimes))

        return ret, _AiSamplingTimes.value

    # @breif
    # @param int Id
    # @param int AiTransferTimes
    # @return int ret
    def SetAiEventTransferTimes(self, Id, AiTransferTimes):
        """
        ### breif
        プロト関数: AioSetAiEventTransferTimes
        ### args
        * _int_ Id
        * _int_ AiTransferTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiTransferTimes = c_long(AiTransferTimes)

        ret = AioSetAiEventTransferTimes(_Id, _AiTransferTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AiTransferTimes
    # @return int ret
    # @return int AiTransferTimes
    def GetAiEventTransferTimes(self, Id, AiTransferTimes):
        """
        ### breif
        プロト関数: AioGetAiEventTransferTimes
        ### args
        * _int_ Id
        * _int_ AiTransferTimes
        \n### return
        * _int_ ret
        * _int_ AiTransferTimes
        """
        _Id = c_short(Id)
        _AiTransferTimes = c_long(AiTransferTimes)

        ret = AioGetAiEventTransferTimes(_Id, byref(_AiTransferTimes))

        return ret, _AiTransferTimes.value

    # @breif
    # @param int Id
    # @return int ret
    def StartAi(self, Id):
        """
        ### breif
        プロト関数: AioStartAi
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioStartAi(_Id)

        return ret

    # @breif
    # @param int Id
    # @param int TimeOut
    # @return int ret
    def StartAiSync(self, Id, TimeOut):
        """
        ### breif
        プロト関数: AioStartAiSync
        ### args
        * _int_ Id
        * _int_ TimeOut
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimeOut = c_long(TimeOut)

        ret = AioStartAiSync(_Id, _TimeOut)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def StopAi(self, Id):
        """
        ### breif
        プロト関数: AioStopAi
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioStopAi(_Id)

        return ret

    # @breif
    # @param int Id
    # @param int AiStatus
    # @return int ret
    # @return int AiStatus
    def GetAiStatus(self, Id, AiStatus):
        """
        ### breif
        プロト関数: AioGetAiStatus
        ### args
        * _int_ Id
        * _int_ AiStatus
        \n### return
        * _int_ ret
        * _int_ AiStatus
        """
        _Id = c_short(Id)
        _AiStatus = c_long(AiStatus)

        ret = AioGetAiStatus(_Id, byref(_AiStatus))

        return ret, _AiStatus.value

    # @breif
    # @param int Id
    # @param int AiSamplingCount
    # @return int ret
    # @return int AiSamplingCount
    def GetAiSamplingCount(self, Id, AiSamplingCount):
        """
        ### breif
        プロト関数: AioGetAiSamplingCount
        ### args
        * _int_ Id
        * _int_ AiSamplingCount
        \n### return
        * _int_ ret
        * _int_ AiSamplingCount
        """
        _Id = c_short(Id)
        _AiSamplingCount = c_long(AiSamplingCount)

        ret = AioGetAiSamplingCount(_Id, byref(_AiSamplingCount))

        return ret, _AiSamplingCount.value

    # @breif
    # @param int Id
    # @param int AiStopTriggerCount
    # @return int ret
    # @return int AiStopTriggerCount
    def GetAiStopTriggerCount(self, Id, AiStopTriggerCount):
        """
        ### breif
        プロト関数: AioGetAiStopTriggerCount
        ### args
        * _int_ Id
        * _int_ AiStopTriggerCount
        \n### return
        * _int_ ret
        * _int_ AiStopTriggerCount
        """
        _Id = c_short(Id)
        _AiStopTriggerCount = c_long(AiStopTriggerCount)

        ret = AioGetAiStopTriggerCount(_Id, byref(_AiStopTriggerCount))

        return ret, _AiStopTriggerCount.value

    # @breif
    # @param int Id
    # @param int AiTransferCount
    # @return int ret
    # @return int AiTransferCount
    def GetAiTransferCount(self, Id, AiTransferCount):
        """
        ### breif
        プロト関数: AioGetAiTransferCount
        ### args
        * _int_ Id
        * _int_ AiTransferCount
        \n### return
        * _int_ ret
        * _int_ AiTransferCount
        """
        _Id = c_short(Id)
        _AiTransferCount = c_long(AiTransferCount)

        ret = AioGetAiTransferCount(_Id, byref(_AiTransferCount))

        return ret, _AiTransferCount.value

    # @breif
    # @param int Id
    # @param int Lap
    # @return int ret
    # @return int Lap
    def GetAiTransferLap(self, Id, Lap):
        """
        ### breif
        プロト関数: AioGetAiTransferLap
        ### args
        * _int_ Id
        * _int_ Lap
        \n### return
        * _int_ ret
        * _int_ Lap
        """
        _Id = c_short(Id)
        _Lap = c_long(Lap)

        ret = AioGetAiTransferLap(_Id, byref(_Lap))

        return ret, _Lap.value

    # @breif
    # @param int Id
    # @param int Count
    # @return int ret
    # @return int Count
    def GetAiStopTriggerTransferCount(self, Id, Count):
        """
        ### breif
        プロト関数: AioGetAiStopTriggerTransferCount
        ### args
        * _int_ Id
        * _int_ Count
        \n### return
        * _int_ ret
        * _int_ Count
        """
        _Id = c_short(Id)
        _Count = c_long(Count)

        ret = AioGetAiStopTriggerTransferCount(_Id, byref(_Count))

        return ret, _Count.value

    # @breif
    # @param int Id
    # @param int AiRepeatCount
    # @return int ret
    # @return int AiRepeatCount
    def GetAiRepeatCount(self, Id, AiRepeatCount):
        """
        ### breif
        プロト関数: AioGetAiRepeatCount
        ### args
        * _int_ Id
        * _int_ AiRepeatCount
        \n### return
        * _int_ ret
        * _int_ AiRepeatCount
        """
        _Id = c_short(Id)
        _AiRepeatCount = c_long(AiRepeatCount)

        ret = AioGetAiRepeatCount(_Id, byref(_AiRepeatCount))

        return ret, _AiRepeatCount.value

    # @breif
    # @param int Id
    # @param int AiSamplingTimes
    # @param int AiData
    # @return int ret
    # @return int AiSamplingTimes
    # @return int AiData
    def GetAiSamplingData(self, Id, AiSamplingTimes, AiData):
        """
        ### breif
        プロト関数: AioGetAiSamplingData
        ### args
        * _int_ Id
        * _int_ AiSamplingTimes
        * _int_ AiData
        \n### return
        * _int_ ret
        * _int_ AiSamplingTimes
        * _int_ AiData
        """
        _Id = c_short(Id)
        _AiSamplingTimes    = c_long(AiSamplingTimes)
        Buf                 = c_long * AiSamplingTimes
        _AiData             = Buf(*AiData)

        ret = AioGetAiSamplingData(_Id, byref(_AiSamplingTimes), byref(_AiData))

        return ret, _AiSamplingTimes.value, _AiData

    # @breif
    # @param int Id
    # @param int AiSamplingTimes
    # @param float AiData
    # @return int ret
    # @return int AiSamplingTimes
    # @return float AiData
    def GetAiSamplingDataEx(self, Id, AiSamplingTimes, AiData):
        """
        ### breif
        プロト関数: AioGetAiSamplingDataEx
        ### args
        * _int_ Id
        * _int_ AiSamplingTimes
        * _float_ AiData
        \n### return
        * _int_ ret
        * _int_ AiSamplingTimes
        * _float_ AiData
        """

        _Id = c_short(Id)
        _AiSamplingTimes = c_long(AiSamplingTimes)
        Buf              = c_float * len(AiData)
        # _AiData          = Buf()
        _AiData          = Buf(*AiData)

        ret = AioGetAiSamplingDataEx(_Id, byref(_AiSamplingTimes), byref(_AiData))

        return ret, _AiSamplingTimes.value, _AiData

    # @breif
    # @param int Id
    # @return int ret
    def ResetAiStatus(self, Id):
        """
        ### breif
        プロト関数: AioResetAiStatus
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioResetAiStatus(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def ResetAiMemory(self, Id):
        """
        ### breif
        プロト関数: AioResetAiMemory
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioResetAiMemory(_Id)

        return ret

    # @breif
    # @param int Id
    # @param int AoChannel
    # @param int AoData
    # @return int ret
    def SingleAo(self, Id, AoChannel, AoData):
        """
        ### breif
        プロト関数: AioSingleAo
        ### args
        * _int_ Id
        * _int_ AoChannel
        * _int_ AoData
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoChannel = c_short(AoChannel)
        _AoData = c_long(AoData)

        ret = AioSingleAo(_Id, _AoChannel, _AoData)

        return ret

    # @breif
    # @param int Id
    # @param int AoChannel
    # @param float AoData
    # @return int ret
    def SingleAoEx(self, Id, AoChannel, AoData):
        """
        ### breif
        プロト関数: AioSingleAoEx
        ### args
        * _int_ Id
        * _int_ AoChannel
        * _float_ AoData
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoChannel = c_short(AoChannel)
        _AoData = c_float(AoData)

        ret = AioSingleAoEx(_Id, _AoChannel, _AoData)

        return ret

    # @breif
    # @param int Id
    # @param int AoChannels
    # @param int AoData
    # @return int ret
    # @return int AoData
    def MultiAo(self, Id, AoChannels, AoData):
        """
        ### breif
        プロト関数: AioMultiAo
        ### args
        * _int_ Id
        * _int_ AoChannels
        * _int_ AoData
        \n### return
        * _int_ ret
        * _int_ AoData
        """
        _Id = c_short(Id)
        _AoChannels = c_short(AoChannels)
        _AoData = c_long(AoData)

        ret = AioMultiAo(_Id, _AoChannels, byref(_AoData))

        return ret, _AoData.value

    # @breif
    # @param int Id
    # @param int AoChannels
    # @param float AoData
    # @return int ret
    # @return float AoData
    def MultiAoEx(self, Id, AoChannels, AoData):
        """
        ### breif
        プロト関数: AioMultiAoEx
        ### args
        * _int_ Id
        * _int_ AoChannels
        * _float_ AoData
        \n### return
        * _int_ ret
        * _float_ AoData
        """
        _Id = c_short(Id)
        _AoChannels = c_short(AoChannels)
        _AoData = c_float(AoData)

        ret = AioMultiAoEx(_Id, _AoChannels, byref(_AoData))

        return ret, _AoData.value

    # @breif
    # @param int Id
    # @param int AoResolution
    # @return int ret
    # @return int AoResolution
    def GetAoResolution(self, Id, AoResolution):
        """
        ### breif
        プロト関数: AioGetAoResolution
        ### args
        * _int_ Id
        * _int_ AoResolution
        \n### return
        * _int_ ret
        * _int_ AoResolution
        """
        _Id = c_short(Id)
        _AoResolution = c_short(AoResolution)

        ret = AioGetAoResolution(_Id, byref(_AoResolution))

        return ret, _AoResolution.value

    # @breif
    # @param int Id
    # @param int AoChannels
    # @return int ret
    def SetAoChannels(self, Id, AoChannels):
        """
        ### breif
        プロト関数: AioSetAoChannels
        ### args
        * _int_ Id
        * _int_ AoChannels
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoChannels = c_short(AoChannels)

        ret = AioSetAoChannels(_Id, _AoChannels)

        return ret

    # @breif
    # @param int Id
    # @param int AoChannels
    # @return int ret
    # @return int AoChannels
    def GetAoChannels(self, Id, AoChannels):
        """
        ### breif
        プロト関数: AioGetAoChannels
        ### args
        * _int_ Id
        * _int_ AoChannels
        \n### return
        * _int_ ret
        * _int_ AoChannels
        """
        _Id = c_short(Id)
        _AoChannels = c_short(AoChannels)

        ret = AioGetAoChannels(_Id, byref(_AoChannels))

        return ret, _AoChannels.value

    # @breif
    # @param int Id
    # @param int AoMaxChannels
    # @return int ret
    # @return int AoMaxChannels
    def GetAoMaxChannels(self, Id, AoMaxChannels):
        """
        ### breif
        プロト関数: AioGetAoMaxChannels
        ### args
        * _int_ Id
        * _int_ AoMaxChannels
        \n### return
        * _int_ ret
        * _int_ AoMaxChannels
        """
        _Id = c_short(Id)
        _AoMaxChannels = c_short(AoMaxChannels)

        ret = AioGetAoMaxChannels(_Id, byref(_AoMaxChannels))

        return ret, _AoMaxChannels.value

    # @breif
    # @param int Id
    # @param int AoChannel
    # @param int AoRange
    # @return int ret
    def SetAoRange(self, Id, AoChannel, AoRange):
        """
        ### breif
        プロト関数: AioSetAoRange
        ### args
        * _int_ Id
        * _int_ AoChannel
        * _int_ AoRange
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoChannel = c_short(AoChannel)
        _AoRange = c_short(AoRange)

        ret = AioSetAoRange(_Id, _AoChannel, _AoRange)

        return ret

    # @breif
    # @param int Id
    # @param int AoRange
    # @return int ret
    def SetAoRangeAll(self, Id, AoRange):
        """
        ### breif
        プロト関数: AioSetAoRangeAll
        ### args
        * _int_ Id
        * _int_ AoRange
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoRange = c_short(AoRange)

        ret = AioSetAoRangeAll(_Id, _AoRange)

        return ret

    # @breif
    # @param int Id
    # @param int AoChannel
    # @param int AoRange
    # @return int ret
    # @return int AoRange
    def GetAoRange(self, Id, AoChannel, AoRange):
        """
        ### breif
        プロト関数: AioGetAoRange
        ### args
        * _int_ Id
        * _int_ AoChannel
        * _int_ AoRange
        \n### return
        * _int_ ret
        * _int_ AoRange
        """
        _Id = c_short(Id)
        _AoChannel = c_short(AoChannel)
        _AoRange = c_short(AoRange)

        ret = AioGetAoRange(_Id, _AoChannel, byref(_AoRange))

        return ret, _AoRange.value

    # @breif
    # @param int Id
    # @param int AoTransferMode
    # @return int ret
    def SetAoTransferMode(self, Id, AoTransferMode):
        """
        ### breif
        プロト関数: AioSetAoTransferMode
        ### args
        * _int_ Id
        * _int_ AoTransferMode
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoTransferMode = c_short(AoTransferMode)

        ret = AioSetAoTransferMode(_Id, _AoTransferMode)

        return ret

    # @breif
    # @param int Id
    # @param int AoTransferMode
    # @return int ret
    # @return int AoTransferMode
    def GetAoTransferMode(self, Id, AoTransferMode):
        """
        ### breif
        プロト関数: AioGetAoTransferMode
        ### args
        * _int_ Id
        * _int_ AoTransferMode
        \n### return
        * _int_ ret
        * _int_ AoTransferMode
        """
        _Id = c_short(Id)
        _AoTransferMode = c_short(AoTransferMode)

        ret = AioGetAoTransferMode(_Id, byref(_AoTransferMode))

        return ret, _AoTransferMode.value

    # @breif
    # @param int Id
    # @param int AoDeviceBufferMode
    # @return int ret
    def SetAoDeviceBufferMode(self, Id, AoDeviceBufferMode):
        """
        ### breif
        プロト関数: AioSetAoDeviceBufferMode
        ### args
        * _int_ Id
        * _int_ AoDeviceBufferMode
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoDeviceBufferMode = c_short(AoDeviceBufferMode)

        ret = AioSetAoDeviceBufferMode(_Id, _AoDeviceBufferMode)

        return ret

    # @breif
    # @param int Id
    # @param int AoDeviceBufferMode
    # @return int ret
    # @return int AoDeviceBufferMode
    def GetAoDeviceBufferMode(self, Id, AoDeviceBufferMode):
        """
        ### breif
        プロト関数: AioGetAoDeviceBufferMode
        ### args
        * _int_ Id
        * _int_ AoDeviceBufferMode
        \n### return
        * _int_ ret
        * _int_ AoDeviceBufferMode
        """
        _Id = c_short(Id)
        _AoDeviceBufferMode = c_short(AoDeviceBufferMode)

        ret = AioGetAoDeviceBufferMode(_Id, byref(_AoDeviceBufferMode))

        return ret, _AoDeviceBufferMode.value

    # @breif
    # @param int Id
    # @param int AoMemorySize
    # @return int ret
    def SetAoMemorySize(self, Id, AoMemorySize):
        """
        ### breif
        プロト関数: AioSetAoMemorySize
        ### args
        * _int_ Id
        * _int_ AoMemorySize
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoMemorySize = c_long(AoMemorySize)

        ret = AioSetAoMemorySize(_Id, _AoMemorySize)

        return ret

    # @breif
    # @param int Id
    # @param int AoMemorySize
    # @return int ret
    # @return int AoMemorySize
    def GetAoMemorySize(self, Id, AoMemorySize):
        """
        ### breif
        プロト関数: AioGetAoMemorySize
        ### args
        * _int_ Id
        * _int_ AoMemorySize
        \n### return
        * _int_ ret
        * _int_ AoMemorySize
        """
        _Id = c_short(Id)
        _AoMemorySize = c_long(AoMemorySize)

        ret = AioGetAoMemorySize(_Id, byref(_AoMemorySize))

        return ret, _AoMemorySize.value

    # @breif
    # @param int Id
    # @param int DataNumber
    # @param int Buffer
    # @return int ret
    # @return int Buffer
    def SetAoTransferData(self, Id, DataNumber, Buffer):
        """
        ### breif
        プロト関数: AioSetAoTransferData
        ### args
        * _int_ Id
        * _int_ DataNumber
        * _int_ Buffer
        \n### return
        * _int_ ret
        * _int_ Buffer
        """
        _Id = c_short(Id)
        _DataNumber = c_long(DataNumber)
        _Buffer = c_long(Buffer)

        ret = AioSetAoTransferData(_Id, _DataNumber, byref(_Buffer))

        return ret, _Buffer.value

    # @breif
    # @param int Id
    # @param int DataSize
    # @return int ret
    # @return int DataSize
    def GetAoSamplingDataSize(self, Id, DataSize):
        """
        ### breif
        プロト関数: AioGetAoSamplingDataSize
        ### args
        * _int_ Id
        * _int_ DataSize
        \n### return
        * _int_ ret
        * _int_ DataSize
        """
        _Id = c_short(Id)
        _DataSize = c_short(DataSize)

        ret = AioGetAoSamplingDataSize(_Id, byref(_DataSize))

        return ret, _DataSize.value

    # @breif
    # @param int Id
    # @param int AoMemoryType
    # @return int ret
    def SetAoMemoryType(self, Id, AoMemoryType):
        """
        ### breif
        プロト関数: AioSetAoMemoryType
        ### args
        * _int_ Id
        * _int_ AoMemoryType
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoMemoryType = c_short(AoMemoryType)

        ret = AioSetAoMemoryType(_Id, _AoMemoryType)

        return ret

    # @breif
    # @param int Id
    # @param int AoMemoryType
    # @return int ret
    # @return int AoMemoryType
    def GetAoMemoryType(self, Id, AoMemoryType):
        """
        ### breif
        プロト関数: AioGetAoMemoryType
        ### args
        * _int_ Id
        * _int_ AoMemoryType
        \n### return
        * _int_ ret
        * _int_ AoMemoryType
        """
        _Id = c_short(Id)
        _AoMemoryType = c_short(AoMemoryType)

        ret = AioGetAoMemoryType(_Id, byref(_AoMemoryType))

        return ret, _AoMemoryType.value

    # @breif
    # @param int Id
    # @param int AoRepeatTimes
    # @return int ret
    def SetAoRepeatTimes(self, Id, AoRepeatTimes):
        """
        ### breif
        プロト関数: AioSetAoRepeatTimes
        ### args
        * _int_ Id
        * _int_ AoRepeatTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoRepeatTimes = c_long(AoRepeatTimes)

        ret = AioSetAoRepeatTimes(_Id, _AoRepeatTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AoRepeatTimes
    # @return int ret
    # @return int AoRepeatTimes
    def GetAoRepeatTimes(self, Id, AoRepeatTimes):
        """
        ### breif
        プロト関数: AioGetAoRepeatTimes
        ### args
        * _int_ Id
        * _int_ AoRepeatTimes
        \n### return
        * _int_ ret
        * _int_ AoRepeatTimes
        """
        _Id = c_short(Id)
        _AoRepeatTimes = c_long(AoRepeatTimes)

        ret = AioGetAoRepeatTimes(_Id, byref(_AoRepeatTimes))

        return ret, _AoRepeatTimes.value

    # @breif
    # @param int Id
    # @param int AoClockType
    # @return int ret
    def SetAoClockType(self, Id, AoClockType):
        """
        ### breif
        プロト関数: AioSetAoClockType
        ### args
        * _int_ Id
        * _int_ AoClockType
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoClockType = c_short(AoClockType)

        ret = AioSetAoClockType(_Id, _AoClockType)

        return ret

    # @breif
    # @param int Id
    # @param int AoClockType
    # @return int ret
    # @return int AoClockType
    def GetAoClockType(self, Id, AoClockType):
        """
        ### breif
        プロト関数: AioGetAoClockType
        ### args
        * _int_ Id
        * _int_ AoClockType
        \n### return
        * _int_ ret
        * _int_ AoClockType
        """
        _Id = c_short(Id)
        _AoClockType = c_short(AoClockType)

        ret = AioGetAoClockType(_Id, byref(_AoClockType))

        return ret, _AoClockType.value

    # @breif
    # @param int Id
    # @param float AoSamplingClock
    # @return int ret
    def SetAoSamplingClock(self, Id, AoSamplingClock):
        """
        ### breif
        プロト関数: AioSetAoSamplingClock
        ### args
        * _int_ Id
        * _float_ AoSamplingClock
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoSamplingClock = c_float(AoSamplingClock)

        ret = AioSetAoSamplingClock(_Id, _AoSamplingClock)

        return ret

    # @breif
    # @param int Id
    # @param float AoSamplingClock
    # @return int ret
    # @return float AoSamplingClock
    def GetAoSamplingClock(self, Id, AoSamplingClock):
        """
        ### breif
        プロト関数: AioGetAoSamplingClock
        ### args
        * _int_ Id
        * _float_ AoSamplingClock
        \n### return
        * _int_ ret
        * _float_ AoSamplingClock
        """
        _Id = c_short(Id)
        _AoSamplingClock = c_float(AoSamplingClock)

        ret = AioGetAoSamplingClock(_Id, byref(_AoSamplingClock))

        return ret, _AoSamplingClock.value

    # @breif
    # @param int Id
    # @param int AoClockEdge
    # @return int ret
    def SetAoClockEdge(self, Id, AoClockEdge):
        """
        ### breif
        プロト関数: AioSetAoClockEdge
        ### args
        * _int_ Id
        * _int_ AoClockEdge
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoClockEdge = c_short(AoClockEdge)

        ret = AioSetAoClockEdge(_Id, _AoClockEdge)

        return ret

    # @breif
    # @param int Id
    # @param int AoClockEdge
    # @return int ret
    # @return int AoClockEdge
    def GetAoClockEdge(self, Id, AoClockEdge):
        """
        ### breif
        プロト関数: AioGetAoClockEdge
        ### args
        * _int_ Id
        * _int_ AoClockEdge
        \n### return
        * _int_ ret
        * _int_ AoClockEdge
        """
        _Id = c_short(Id)
        _AoClockEdge = c_short(AoClockEdge)

        ret = AioGetAoClockEdge(_Id, byref(_AoClockEdge))

        return ret, _AoClockEdge.value

    # @breif
    # @param int Id
    # @param int AoSamplingTimes
    # @param int AoData
    # @return int ret
    # @return int AoData
    def SetAoSamplingData(self, Id, AoSamplingTimes, AoData):
        """
        ### breif
        プロト関数: AioSetAoSamplingData
        ### args
        * _int_ Id
        * _int_ AoSamplingTimes
        * _int_ AoData
        \n### return
        * _int_ ret
        * _int_ AoData
        """
        _Id = c_short(Id)
        _AoSamplingTimes = c_long(AoSamplingTimes)
        _AoData = c_long(AoData)

        ret = AioSetAoSamplingData(_Id, _AoSamplingTimes, byref(_AoData))

        return ret, _AoData.value

    # @breif
    # @param int Id
    # @param int AoSamplingTimes
    # @param float AoData
    # @return int ret
    # @return float AoData
    def SetAoSamplingDataEx(self, Id, AoSamplingTimes, AoData):
        """
        ### breif
        プロト関数: AioSetAoSamplingDataEx
        ### args
        * _int_ Id
        * _int_ AoSamplingTimes
        * _float_ AoData
        \n### return
        * _int_ ret
        * _float_ AoData
        """
        _Id = c_short(Id)
        _AoSamplingTimes = c_long(AoSamplingTimes)
        _AoData = c_float(AoData)

        ret = AioSetAoSamplingDataEx(_Id, _AoSamplingTimes, byref(_AoData))

        return ret, _AoData.value

    # @breif
    # @param int Id
    # @param int AoSamplingTimes
    # @return int ret
    # @return int AoSamplingTimes
    def GetAoSamplingTimes(self, Id, AoSamplingTimes):
        """
        ### breif
        プロト関数: AioGetAoSamplingTimes
        ### args
        * _int_ Id
        * _int_ AoSamplingTimes
        \n### return
        * _int_ ret
        * _int_ AoSamplingTimes
        """
        _Id = c_short(Id)
        _AoSamplingTimes = c_long(AoSamplingTimes)

        ret = AioGetAoSamplingTimes(_Id, byref(_AoSamplingTimes))

        return ret, _AoSamplingTimes.value

    # @breif
    # @param int Id
    # @param int AoStartTrigger
    # @return int ret
    def SetAoStartTrigger(self, Id, AoStartTrigger):
        """
        ### breif
        プロト関数: AioSetAoStartTrigger
        ### args
        * _int_ Id
        * _int_ AoStartTrigger
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoStartTrigger = c_short(AoStartTrigger)

        ret = AioSetAoStartTrigger(_Id, _AoStartTrigger)

        return ret

    # @breif
    # @param int Id
    # @param int AoStartTrigger
    # @return int ret
    # @return int AoStartTrigger
    def GetAoStartTrigger(self, Id, AoStartTrigger):
        """
        ### breif
        プロト関数: AioGetAoStartTrigger
        ### args
        * _int_ Id
        * _int_ AoStartTrigger
        \n### return
        * _int_ ret
        * _int_ AoStartTrigger
        """
        _Id = c_short(Id)
        _AoStartTrigger = c_short(AoStartTrigger)

        ret = AioGetAoStartTrigger(_Id, byref(_AoStartTrigger))

        return ret, _AoStartTrigger.value

    # @breif
    # @param int Id
    # @param int AoStopTrigger
    # @return int ret
    def SetAoStopTrigger(self, Id, AoStopTrigger):
        """
        ### breif
        プロト関数: AioSetAoStopTrigger
        ### args
        * _int_ Id
        * _int_ AoStopTrigger
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoStopTrigger = c_short(AoStopTrigger)

        ret = AioSetAoStopTrigger(_Id, _AoStopTrigger)

        return ret

    # @breif
    # @param int Id
    # @param int AoStopTrigger
    # @return int ret
    # @return int AoStopTrigger
    def GetAoStopTrigger(self, Id, AoStopTrigger):
        """
        ### breif
        プロト関数: AioGetAoStopTrigger
        ### args
        * _int_ Id
        * _int_ AoStopTrigger
        \n### return
        * _int_ ret
        * _int_ AoStopTrigger
        """
        _Id = c_short(Id)
        _AoStopTrigger = c_short(AoStopTrigger)

        ret = AioGetAoStopTrigger(_Id, byref(_AoStopTrigger))

        return ret, _AoStopTrigger.value

    # @breif
    # @param int Id
    # @param int AoEvent
    # @return int ret
    def SetAoEvent(self, Id, AoEvent):
        """
        ### breif
        プロト関数: AioSetAoEvent
        ### args
        * _int_ Id
        * _int_ AoEvent
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoEvent = c_long(AoEvent)

        ret = AioSetAoEvent(_Id, _AoEvent)

        return ret

    # @breif
    # @param int Id
    # @param int AoEvent
    # @return int ret
    # @return int AoEvent
    def GetAoEvent(self, Id, AoEvent):
        """
        ### breif
        プロト関数: AioGetAoEvent
        ### args
        * _int_ Id
        * _int_ AoEvent
        \n### return
        * _int_ ret
        * _int_ AoEvent
        """
        _Id = c_short(Id)
        _AoEvent = c_long(AoEvent)

        ret = AioGetAoEvent(_Id, byref(_AoEvent))

        return ret, _AoEvent.value

    # @breif
    # @param int Id
    # @param int AoSamplingTimes
    # @return int ret
    def SetAoEventSamplingTimes(self, Id, AoSamplingTimes):
        """
        ### breif
        プロト関数: AioSetAoEventSamplingTimes
        ### args
        * _int_ Id
        * _int_ AoSamplingTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoSamplingTimes = c_long(AoSamplingTimes)

        ret = AioSetAoEventSamplingTimes(_Id, _AoSamplingTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AoSamplingTimes
    # @return int ret
    # @return int AoSamplingTimes
    def GetAoEventSamplingTimes(self, Id, AoSamplingTimes):
        """
        ### breif
        プロト関数: AioGetAoEventSamplingTimes
        ### args
        * _int_ Id
        * _int_ AoSamplingTimes
        \n### return
        * _int_ ret
        * _int_ AoSamplingTimes
        """
        _Id = c_short(Id)
        _AoSamplingTimes = c_long(AoSamplingTimes)

        ret = AioGetAoEventSamplingTimes(_Id, byref(_AoSamplingTimes))

        return ret, _AoSamplingTimes.value

    # @breif
    # @param int Id
    # @param int AoTransferTimes
    # @return int ret
    def SetAoEventTransferTimes(self, Id, AoTransferTimes):
        """
        ### breif
        プロト関数: AioSetAoEventTransferTimes
        ### args
        * _int_ Id
        * _int_ AoTransferTimes
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoTransferTimes = c_long(AoTransferTimes)

        ret = AioSetAoEventTransferTimes(_Id, _AoTransferTimes)

        return ret

    # @breif
    # @param int Id
    # @param int AoTransferTimes
    # @return int ret
    # @return int AoTransferTimes
    def GetAoEventTransferTimes(self, Id, AoTransferTimes):
        """
        ### breif
        プロト関数: AioGetAoEventTransferTimes
        ### args
        * _int_ Id
        * _int_ AoTransferTimes
        \n### return
        * _int_ ret
        * _int_ AoTransferTimes
        """
        _Id = c_short(Id)
        _AoTransferTimes = c_long(AoTransferTimes)

        ret = AioGetAoEventTransferTimes(_Id, byref(_AoTransferTimes))

        return ret, _AoTransferTimes.value

    # @breif
    # @param int Id
    # @return int ret
    def StartAo(self, Id):
        """
        ### breif
        プロト関数: AioStartAo
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioStartAo(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def StopAo(self, Id):
        """
        ### breif
        プロト関数: AioStopAo
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioStopAo(_Id)

        return ret

    # @breif
    # @param int Id
    # @param int AoChannel
    # @return int ret
    def EnableAo(self, Id, AoChannel):
        """
        ### breif
        プロト関数: AioEnableAo
        ### args
        * _int_ Id
        * _int_ AoChannel
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoChannel = c_short(AoChannel)

        ret = AioEnableAo(_Id, _AoChannel)

        return ret

    # @breif
    # @param int Id
    # @param int AoChannel
    # @return int ret
    def DisableAo(self, Id, AoChannel):
        """
        ### breif
        プロト関数: AioDisableAo
        ### args
        * _int_ Id
        * _int_ AoChannel
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AoChannel = c_short(AoChannel)

        ret = AioDisableAo(_Id, _AoChannel)

        return ret

    # @breif
    # @param int Id
    # @param int AoStatus
    # @return int ret
    # @return int AoStatus
    def GetAoStatus(self, Id, AoStatus):
        """
        ### breif
        プロト関数: AioGetAoStatus
        ### args
        * _int_ Id
        * _int_ AoStatus
        \n### return
        * _int_ ret
        * _int_ AoStatus
        """
        _Id = c_short(Id)
        _AoStatus = c_long(AoStatus)

        ret = AioGetAoStatus(_Id, byref(_AoStatus))

        return ret, _AoStatus.value

    # @breif
    # @param int Id
    # @param int AoSamplingCount
    # @return int ret
    # @return int AoSamplingCount
    def GetAoSamplingCount(self, Id, AoSamplingCount):
        """
        ### breif
        プロト関数: AioGetAoSamplingCount
        ### args
        * _int_ Id
        * _int_ AoSamplingCount
        \n### return
        * _int_ ret
        * _int_ AoSamplingCount
        """
        _Id = c_short(Id)
        _AoSamplingCount = c_long(AoSamplingCount)

        ret = AioGetAoSamplingCount(_Id, byref(_AoSamplingCount))

        return ret, _AoSamplingCount.value

    # @breif
    # @param int Id
    # @param int AoTransferCount
    # @return int ret
    # @return int AoTransferCount
    def GetAoTransferCount(self, Id, AoTransferCount):
        """
        ### breif
        プロト関数: AioGetAoTransferCount
        ### args
        * _int_ Id
        * _int_ AoTransferCount
        \n### return
        * _int_ ret
        * _int_ AoTransferCount
        """
        _Id = c_short(Id)
        _AoTransferCount = c_long(AoTransferCount)

        ret = AioGetAoTransferCount(_Id, byref(_AoTransferCount))

        return ret, _AoTransferCount.value

    # @breif
    # @param int Id
    # @param int Lap
    # @return int ret
    # @return int Lap
    def GetAoTransferLap(self, Id, Lap):
        """
        ### breif
        プロト関数: AioGetAoTransferLap
        ### args
        * _int_ Id
        * _int_ Lap
        \n### return
        * _int_ ret
        * _int_ Lap
        """
        _Id = c_short(Id)
        _Lap = c_long(Lap)

        ret = AioGetAoTransferLap(_Id, byref(_Lap))

        return ret, _Lap.value

    # @breif
    # @param int Id
    # @param int AoRepeatCount
    # @return int ret
    # @return int AoRepeatCount
    def GetAoRepeatCount(self, Id, AoRepeatCount):
        """
        ### breif
        プロト関数: AioGetAoRepeatCount
        ### args
        * _int_ Id
        * _int_ AoRepeatCount
        \n### return
        * _int_ ret
        * _int_ AoRepeatCount
        """
        _Id = c_short(Id)
        _AoRepeatCount = c_long(AoRepeatCount)

        ret = AioGetAoRepeatCount(_Id, byref(_AoRepeatCount))

        return ret, _AoRepeatCount.value

    # @breif
    # @param int Id
    # @return int ret
    def ResetAoStatus(self, Id):
        """
        ### breif
        プロト関数: AioResetAoStatus
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioResetAoStatus(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def ResetAoMemory(self, Id):
        """
        ### breif
        プロト関数: AioResetAoMemory
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioResetAoMemory(_Id)

        return ret

    # @breif
    # @param int Id
    # @param int Bit
    # @param float Value
    # @return int ret
    def SetDiFilter(self, Id, Bit, Value):
        """
        ### breif
        プロト関数: AioSetDiFilter
        ### args
        * _int_ Id
        * _int_ Bit
        * _float_ Value
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _Bit = c_short(Bit)
        _Value = c_float(Value)

        ret = AioSetDiFilter(_Id, _Bit, _Value)

        return ret

    # @breif
    # @param int Id
    # @param int Bit
    # @param float Value
    # @return int ret
    # @return float Value
    def GetDiFilter(self, Id, Bit, Value):
        """
        ### breif
        プロト関数: AioGetDiFilter
        ### args
        * _int_ Id
        * _int_ Bit
        * _float_ Value
        \n### return
        * _int_ ret
        * _float_ Value
        """
        _Id = c_short(Id)
        _Bit = c_short(Bit)
        _Value = c_float(Value)

        ret = AioGetDiFilter(_Id, _Bit, byref(_Value))

        return ret, _Value.value

    # @breif
    # @param int Id
    # @param int DiBit
    # @param int DiData
    # @return int ret
    # @return int DiData
    def InputDiBit(self, Id, DiBit, DiData):
        """
        ### breif
        プロト関数: AioInputDiBit
        ### args
        * _int_ Id
        * _int_ DiBit
        * _int_ DiData
        \n### return
        * _int_ ret
        * _int_ DiData
        """
        _Id = c_short(Id)
        _DiBit = c_short(DiBit)
        _DiData = c_short(DiData)

        ret = AioInputDiBit(_Id, _DiBit, byref(_DiData))

        return ret, _DiData.value

    # @breif
    # @param int Id
    # @param int DoBit
    # @param int DoData
    # @return int ret
    def OutputDoBit(self, Id, DoBit, DoData):
        """
        ### breif
        プロト関数: AioOutputDoBit
        ### args
        * _int_ Id
        * _int_ DoBit
        * _int_ DoData
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _DoBit = c_short(DoBit)
        _DoData = c_short(DoData)

        ret = AioOutputDoBit(_Id, _DoBit, _DoData)

        return ret

    # @breif
    # @param int Id
    # @param int DiPort
    # @param int DiData
    # @return int ret
    # @return int DiData
    def InputDiByte(self, Id, DiPort, DiData):
        """
        ### breif
        プロト関数: AioInputDiByte
        ### args
        * _int_ Id
        * _int_ DiPort
        * _int_ DiData
        \n### return
        * _int_ ret
        * _int_ DiData
        """
        _Id = c_short(Id)
        _DiPort = c_short(DiPort)
        _DiData = c_short(DiData)

        ret = AioInputDiByte(_Id, _DiPort, byref(_DiData))

        return ret, _DiData.value

    # @breif
    # @param int Id
    # @param int DoPort
    # @param int DoData
    # @return int ret
    def OutputDoByte(self, Id, DoPort, DoData):
        """
        ### breif
        プロト関数: AioOutputDoByte
        ### args
        * _int_ Id
        * _int_ DoPort
        * _int_ DoData
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _DoPort = c_short(DoPort)
        _DoData = c_short(DoData)

        ret = AioOutputDoByte(_Id, _DoPort, _DoData)

        return ret

    # @breif
    # @param int Id
    # @param int Dir
    # @return int ret
    def SetDioDirection(self, Id, Dir):
        """
        ### breif
        プロト関数: AioSetDioDirection
        ### args
        * _int_ Id
        * _int_ Dir
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _Dir = c_long(Dir)

        ret = AioSetDioDirection(_Id, _Dir)

        return ret

    # @breif
    # @param int Id
    # @param int CntMaxChannels
    # @return int ret
    # @return int CntMaxChannels
    def GetCntMaxChannels(self, Id, CntMaxChannels):
        """
        ### breif
        プロト関数: AioGetCntMaxChannels
        ### args
        * _int_ Id
        * _int_ CntMaxChannels
        \n### return
        * _int_ ret
        * _int_ CntMaxChannels
        """
        _Id = c_short(Id)
        _CntMaxChannels = c_short(CntMaxChannels)

        ret = AioGetCntMaxChannels(_Id, byref(_CntMaxChannels))

        return ret, _CntMaxChannels.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int CntMode
    # @return int ret
    def SetCntComparisonMode(self, Id, CntChannel, CntMode):
        """
        ### breif
        プロト関数: AioSetCntComparisonMode
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ CntMode
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _CntMode = c_short(CntMode)

        ret = AioSetCntComparisonMode(_Id, _CntChannel, _CntMode)

        return ret

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int CntMode
    # @return int ret
    # @return int CntMode
    def GetCntComparisonMode(self, Id, CntChannel, CntMode):
        """
        ### breif
        プロト関数: AioGetCntComparisonMode
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ CntMode
        \n### return
        * _int_ ret
        * _int_ CntMode
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _CntMode = c_short(CntMode)

        ret = AioGetCntComparisonMode(_Id, _CntChannel, byref(_CntMode))

        return ret, _CntMode.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int PresetNumber
    # @param int PresetData
    # @param int Flag
    # @return int ret
    # @return int PresetData
    def SetCntPresetReg(self, Id, CntChannel, PresetNumber, PresetData, Flag):
        """
        ### breif
        プロト関数: AioSetCntPresetReg
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ PresetNumber
        * _int_ PresetData
        * _int_ Flag
        \n### return
        * _int_ ret
        * _int_ PresetData
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _PresetNumber = c_long(PresetNumber)
        _PresetData = c_long(PresetData)
        _Flag = c_short(Flag)

        ret = AioSetCntPresetReg(_Id, _CntChannel, _PresetNumber, byref(_PresetData), _Flag)

        return ret, _PresetData.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int ComparisonNumber
    # @param int ComparisonData
    # @param int Flag
    # @return int ret
    # @return int ComparisonData
    def SetCntComparisonReg(self, Id, CntChannel, ComparisonNumber, ComparisonData, Flag):
        """
        ### breif
        プロト関数: AioSetCntComparisonReg
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ ComparisonNumber
        * _int_ ComparisonData
        * _int_ Flag
        \n### return
        * _int_ ret
        * _int_ ComparisonData
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _ComparisonNumber = c_long(ComparisonNumber)
        _ComparisonData = c_long(ComparisonData)
        _Flag = c_short(Flag)

        ret = AioSetCntComparisonReg(_Id, _CntChannel, _ComparisonNumber, byref(_ComparisonData), _Flag)

        return ret, _ComparisonData.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int CntInputSignal
    # @return int ret
    def SetCntInputSignal(self, Id, CntChannel, CntInputSignal):
        """
        ### breif
        プロト関数: AioSetCntInputSignal
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ CntInputSignal
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _CntInputSignal = c_short(CntInputSignal)

        ret = AioSetCntInputSignal(_Id, _CntChannel, _CntInputSignal)

        return ret

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int CntInputSignal
    # @return int ret
    # @return int CntInputSignal
    def GetCntInputSignal(self, Id, CntChannel, CntInputSignal):
        """
        ### breif
        プロト関数: AioGetCntInputSignal
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ CntInputSignal
        \n### return
        * _int_ ret
        * _int_ CntInputSignal
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _CntInputSignal = c_short(CntInputSignal)

        ret = AioGetCntInputSignal(_Id, _CntChannel, byref(_CntInputSignal))

        return ret, _CntInputSignal.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int CntEvent
    # @return int ret
    def SetCntEvent(self, Id, CntChannel, CntEvent):
        """
        ### breif
        プロト関数: AioSetCntEvent
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ CntEvent
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _CntEvent = c_long(CntEvent)

        ret = AioSetCntEvent(_Id, _CntChannel, _CntEvent)

        return ret

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int CntEvent
    # @return int ret
    # @return int CntEvent
    def GetCntEvent(self, Id, CntChannel, CntEvent):
        """
        ### breif
        プロト関数: AioGetCntEvent
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ CntEvent
        \n### return
        * _int_ ret
        * _int_ CntEvent
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _CntEvent = c_long(CntEvent)

        ret = AioGetCntEvent(_Id, _CntChannel, byref(_CntEvent))

        return ret, _CntEvent.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int Signal
    # @param float Value
    # @return int ret
    def SetCntFilter(self, Id, CntChannel, Signal, Value):
        """
        ### breif
        プロト関数: AioSetCntFilter
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ Signal
        * _float_ Value
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _Signal = c_short(Signal)
        _Value = c_float(Value)

        ret = AioSetCntFilter(_Id, _CntChannel, _Signal, _Value)

        return ret

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int Signal
    # @param float Value
    # @return int ret
    # @return float Value
    def GetCntFilter(self, Id, CntChannel, Signal, Value):
        """
        ### breif
        プロト関数: AioGetCntFilter
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ Signal
        * _float_ Value
        \n### return
        * _int_ ret
        * _float_ Value
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _Signal = c_short(Signal)
        _Value = c_float(Value)

        ret = AioGetCntFilter(_Id, _CntChannel, _Signal, byref(_Value))

        return ret, _Value.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @return int ret
    def StartCnt(self, Id, CntChannel):
        """
        ### breif
        プロト関数: AioStartCnt
        ### args
        * _int_ Id
        * _int_ CntChannel
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)

        ret = AioStartCnt(_Id, _CntChannel)

        return ret

    # @breif
    # @param int Id
    # @param int CntChannel
    # @return int ret
    def StopCnt(self, Id, CntChannel):
        """
        ### breif
        プロト関数: AioStopCnt
        ### args
        * _int_ Id
        * _int_ CntChannel
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)

        ret = AioStopCnt(_Id, _CntChannel)

        return ret

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int PresetData
    # @return int ret
    def PresetCnt(self, Id, CntChannel, PresetData):
        """
        ### breif
        プロト関数: AioPresetCnt
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ PresetData
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _PresetData = c_long(PresetData)

        ret = AioPresetCnt(_Id, _CntChannel, _PresetData)

        return ret

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int CntStatus
    # @return int ret
    # @return int CntStatus
    def GetCntStatus(self, Id, CntChannel, CntStatus):
        """
        ### breif
        プロト関数: AioGetCntStatus
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ CntStatus
        \n### return
        * _int_ ret
        * _int_ CntStatus
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _CntStatus = c_long(CntStatus)

        ret = AioGetCntStatus(_Id, _CntChannel, byref(_CntStatus))

        return ret, _CntStatus.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int Count
    # @return int ret
    # @return int Count
    def GetCntCount(self, Id, CntChannel, Count):
        """
        ### breif
        プロト関数: AioGetCntCount
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ Count
        \n### return
        * _int_ ret
        * _int_ Count
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _Count = c_long(Count)

        ret = AioGetCntCount(_Id, _CntChannel, byref(_Count))

        return ret, _Count.value

    # @breif
    # @param int Id
    # @param int CntChannel
    # @param int CntStatus
    # @return int ret
    def ResetCntStatus(self, Id, CntChannel, CntStatus):
        """
        ### breif
        プロト関数: AioResetCntStatus
        ### args
        * _int_ Id
        * _int_ CntChannel
        * _int_ CntStatus
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntChannel = c_short(CntChannel)
        _CntStatus = c_long(CntStatus)

        ret = AioResetCntStatus(_Id, _CntChannel, _CntStatus)

        return ret

    # @breif
    # @param int Id
    # @param int TimerId
    # @param int TmEvent
    # @return int ret
    def SetTmEvent(self, Id, TimerId, TmEvent):
        """
        ### breif
        プロト関数: AioSetTmEvent
        ### args
        * _int_ Id
        * _int_ TimerId
        * _int_ TmEvent
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)
        _TmEvent = c_long(TmEvent)

        ret = AioSetTmEvent(_Id, _TimerId, _TmEvent)

        return ret

    # @breif
    # @param int Id
    # @param int TimerId
    # @param int TmEvent
    # @return int ret
    # @return int TmEvent
    def GetTmEvent(self, Id, TimerId, TmEvent):
        """
        ### breif
        プロト関数: AioGetTmEvent
        ### args
        * _int_ Id
        * _int_ TimerId
        * _int_ TmEvent
        \n### return
        * _int_ ret
        * _int_ TmEvent
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)
        _TmEvent = c_long(TmEvent)

        ret = AioGetTmEvent(_Id, _TimerId, byref(_TmEvent))

        return ret, _TmEvent.value

    # @breif
    # @param int Id
    # @param int TimerId
    # @param float Interval
    # @return int ret
    def StartTmTimer(self, Id, TimerId, Interval):
        """
        ### breif
        プロト関数: AioStartTmTimer
        ### args
        * _int_ Id
        * _int_ TimerId
        * _float_ Interval
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)
        _Interval = c_float(Interval)

        ret = AioStartTmTimer(_Id, _TimerId, _Interval)

        return ret

    # @breif
    # @param int Id
    # @param int TimerId
    # @return int ret
    def StopTmTimer(self, Id, TimerId):
        """
        ### breif
        プロト関数: AioStopTmTimer
        ### args
        * _int_ Id
        * _int_ TimerId
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)

        ret = AioStopTmTimer(_Id, _TimerId)

        return ret

    # @breif
    # @param int Id
    # @param int TimerId
    # @return int ret
    def StartTmCount(self, Id, TimerId):
        """
        ### breif
        プロト関数: AioStartTmCount
        ### args
        * _int_ Id
        * _int_ TimerId
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)

        ret = AioStartTmCount(_Id, _TimerId)

        return ret

    # @breif
    # @param int Id
    # @param int TimerId
    # @return int ret
    def StopTmCount(self, Id, TimerId):
        """
        ### breif
        プロト関数: AioStopTmCount
        ### args
        * _int_ Id
        * _int_ TimerId
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)

        ret = AioStopTmCount(_Id, _TimerId)

        return ret

    # @breif
    # @param int Id
    # @param int TimerId
    # @param int Lap
    # @return int ret
    # @return int Lap
    def LapTmCount(self, Id, TimerId, Lap):
        """
        ### breif
        プロト関数: AioLapTmCount
        ### args
        * _int_ Id
        * _int_ TimerId
        * _int_ Lap
        \n### return
        * _int_ ret
        * _int_ Lap
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)
        _Lap = c_long(Lap)

        ret = AioLapTmCount(_Id, _TimerId, byref(_Lap))

        return ret, _Lap.value

    # @breif
    # @param int Id
    # @param int TimerId
    # @return int ret
    def ResetTmCount(self, Id, TimerId):
        """
        ### breif
        プロト関数: AioResetTmCount
        ### args
        * _int_ Id
        * _int_ TimerId
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)

        ret = AioResetTmCount(_Id, _TimerId)

        return ret

    # @breif
    # @param int Id
    # @param int TimerId
    # @param int Wait
    # @return int ret
    def TmWait(self, Id, TimerId, Wait):
        """
        ### breif
        プロト関数: AioTmWait
        ### args
        * _int_ Id
        * _int_ TimerId
        * _int_ Wait
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimerId = c_short(TimerId)
        _Wait = c_long(Wait)

        ret = AioTmWait(_Id, _TimerId, _Wait)

        return ret

    # @breif
    # @param int Id
    # @param int Destination
    # @param int Source
    # @return int ret
    def SetEcuSignal(self, Id, Destination, Source):
        """
        ### breif
        プロト関数: AioSetEcuSignal
        ### args
        * _int_ Id
        * _int_ Destination
        * _int_ Source
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _Destination = c_short(Destination)
        _Source = c_short(Source)

        ret = AioSetEcuSignal(_Id, _Destination, _Source)

        return ret

    # @breif
    # @param int Id
    # @param int Destination
    # @param int Source
    # @return int ret
    # @return int Source
    def GetEcuSignal(self, Id, Destination, Source):
        """
        ### breif
        プロト関数: AioGetEcuSignal
        ### args
        * _int_ Id
        * _int_ Destination
        * _int_ Source
        \n### return
        * _int_ ret
        * _int_ Source
        """
        _Id = c_short(Id)
        _Destination = c_short(Destination)
        _Source = c_short(Source)

        ret = AioGetEcuSignal(_Id, _Destination, byref(_Source))

        return ret, _Source.value

    # @breif
    # @param int Id
    # @param int CntmMaxChannels
    # @return int ret
    # @return int CntmMaxChannels
    def GetCntmMaxChannels(self, Id, CntmMaxChannels):
        """
        ### breif
        プロト関数: AioGetCntmMaxChannels
        ### args
        * _int_ Id
        * _int_ CntmMaxChannels
        \n### return
        * _int_ ret
        * _int_ CntmMaxChannels
        """
        _Id = c_short(Id)
        _CntmMaxChannels = c_short(CntmMaxChannels)

        ret = AioGetCntmMaxChannels(_Id, byref(_CntmMaxChannels))

        return ret, _CntmMaxChannels.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int Mode
    # @return int ret
    def SetCntmZMode(self, Id, ChNo, Mode):
        """
        ### breif
        プロト関数: AioSetCntmZMode
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ Mode
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _Mode = c_short(Mode)

        ret = AioSetCntmZMode(_Id, _ChNo, _Mode)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int ZLogic
    # @return int ret
    def SetCntmZLogic(self, Id, ChNo, ZLogic):
        """
        ### breif
        プロト関数: AioSetCntmZLogic
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ ZLogic
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _ZLogic = c_short(ZLogic)

        ret = AioSetCntmZLogic(_Id, _ChNo, _ZLogic)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int SigType
    # @return int ret
    def SelectCntmChannelSignal(self, Id, ChNo, SigType):
        """
        ### breif
        プロト関数: AioSelectCntmChannelSignal
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ SigType
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _SigType = c_short(SigType)

        ret = AioSelectCntmChannelSignal(_Id, _ChNo, _SigType)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int Dir
    # @return int ret
    def SetCntmCountDirection(self, Id, ChNo, Dir):
        """
        ### breif
        プロト関数: AioSetCntmCountDirection
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ Dir
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _Dir = c_short(Dir)

        ret = AioSetCntmCountDirection(_Id, _ChNo, _Dir)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int Phase
    # @param int Mul
    # @param int SyncClr
    # @return int ret
    def SetCntmOperationMode(self, Id, ChNo, Phase, Mul, SyncClr):
        """
        ### breif
        プロト関数: AioSetCntmOperationMode
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ Phase
        * _int_ Mul
        * _int_ SyncClr
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _Phase = c_short(Phase)
        _Mul = c_short(Mul)
        _SyncClr = c_short(SyncClr)

        ret = AioSetCntmOperationMode(_Id, _ChNo, _Phase, _Mul, _SyncClr)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int FilterValue
    # @return int ret
    def SetCntmDigitalFilter(self, Id, ChNo, FilterValue):
        """
        ### breif
        プロト関数: AioSetCntmDigitalFilter
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ FilterValue
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _FilterValue = c_short(FilterValue)

        ret = AioSetCntmDigitalFilter(_Id, _ChNo, _FilterValue)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int PlsWidth
    # @return int ret
    def SetCntmPulseWidth(self, Id, ChNo, PlsWidth):
        """
        ### breif
        プロト関数: AioSetCntmPulseWidth
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ PlsWidth
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _PlsWidth = c_short(PlsWidth)

        ret = AioSetCntmPulseWidth(_Id, _ChNo, _PlsWidth)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int OutputLogic
    # @param int EventType
    # @param int PulseWidth
    # @return int ret
    def SetCntmOutputHardwareEvent(self, Id, ChNo, OutputLogic, EventType, PulseWidth):
        """
        ### breif
        プロト関数: AioSetCntmOutputHardwareEvent
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ OutputLogic
        * _int_ EventType
        * _int_ PulseWidth
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _OutputLogic = c_short(OutputLogic)
        _EventType = c_ulong(EventType)
        _PulseWidth = c_short(PulseWidth)

        ret = AioSetCntmOutputHardwareEvent(_Id, _ChNo, _OutputLogic, _EventType, _PulseWidth)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int EventType
    # @param int RF0
    # @param int RF1
    # @param int Reserved
    # @return int ret
    def SetCntmInputHardwareEvent(self, Id, ChNo, EventType, RF0, RF1, Reserved):
        """
        ### breif
        プロト関数: AioSetCntmInputHardwareEvent
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ EventType
        * _int_ RF0
        * _int_ RF1
        * _int_ Reserved
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _EventType = c_ulong(EventType)
        _RF0 = c_short(RF0)
        _RF1 = c_short(RF1)
        _Reserved = c_short(Reserved)

        ret = AioSetCntmInputHardwareEvent(_Id, _ChNo, _EventType, _RF0, _RF1, _Reserved)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int RegisterNo
    # @param int EventType
    # @param int Reserved
    # @return int ret
    def SetCntmCountMatchHardwareEvent(self, Id, ChNo, RegisterNo, EventType, Reserved):
        """
        ### breif
        プロト関数: AioSetCntmCountMatchHardwareEvent
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ RegisterNo
        * _int_ EventType
        * _int_ Reserved
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _RegisterNo = c_short(RegisterNo)
        _EventType = c_ulong(EventType)
        _Reserved = c_short(Reserved)

        ret = AioSetCntmCountMatchHardwareEvent(_Id, _ChNo, _RegisterNo, _EventType, _Reserved)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int PresetData
    # @param int Reserved
    # @return int ret
    def SetCntmPresetRegister(self, Id, ChNo, PresetData, Reserved):
        """
        ### breif
        プロト関数: AioSetCntmPresetRegister
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ PresetData
        * _int_ Reserved
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _PresetData = c_ulong(PresetData)
        _Reserved = c_short(Reserved)

        ret = AioSetCntmPresetRegister(_Id, _ChNo, _PresetData, _Reserved)

        return ret

    # @breif
    # @param int Id
    # @param int CntmInternal
    # @param int CntmOut
    # @param int CntmReserved
    # @return int ret
    def SetCntmTestPulse(self, Id, CntmInternal, CntmOut, CntmReserved):
        """
        ### breif
        プロト関数: AioSetCntmTestPulse
        ### args
        * _int_ Id
        * _int_ CntmInternal
        * _int_ CntmOut
        * _int_ CntmReserved
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _CntmInternal = c_short(CntmInternal)
        _CntmOut = c_short(CntmOut)
        _CntmReserved = c_short(CntmReserved)

        ret = AioSetCntmTestPulse(_Id, _CntmInternal, _CntmOut, _CntmReserved)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int Mode
    # @return int ret
    # @return int Mode
    def GetCntmZMode(self, Id, ChNo, Mode):
        """
        ### breif
        プロト関数: AioGetCntmZMode
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ Mode
        \n### return
        * _int_ ret
        * _int_ Mode
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _Mode = c_short(Mode)

        ret = AioGetCntmZMode(_Id, _ChNo, byref(_Mode))

        return ret, _Mode.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int ZLogic
    # @return int ret
    # @return int ZLogic
    def GetCntmZLogic(self, Id, ChNo, ZLogic):
        """
        ### breif
        プロト関数: AioGetCntmZLogic
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ ZLogic
        \n### return
        * _int_ ret
        * _int_ ZLogic
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _ZLogic = c_short(ZLogic)

        ret = AioGetCntmZLogic(_Id, _ChNo, byref(_ZLogic))

        return ret, _ZLogic.value

    # @breif
    # @param int Id
    # @param int CntmChNo
    # @param int CntmSigType
    # @return int ret
    # @return int CntmSigType
    def GetCntmChannelSignal(self, Id, CntmChNo, CntmSigType):
        """
        ### breif
        プロト関数: AioGetCntmChannelSignal
        ### args
        * _int_ Id
        * _int_ CntmChNo
        * _int_ CntmSigType
        \n### return
        * _int_ ret
        * _int_ CntmSigType
        """
        _Id = c_short(Id)
        _CntmChNo = c_short(CntmChNo)
        _CntmSigType = c_short(CntmSigType)

        ret = AioGetCntmChannelSignal(_Id, _CntmChNo, byref(_CntmSigType))

        return ret, _CntmSigType.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int Dir
    # @return int ret
    # @return int Dir
    def GetCntmCountDirection(self, Id, ChNo, Dir):
        """
        ### breif
        プロト関数: AioGetCntmCountDirection
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ Dir
        \n### return
        * _int_ ret
        * _int_ Dir
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _Dir = c_short(Dir)

        ret = AioGetCntmCountDirection(_Id, _ChNo, byref(_Dir))

        return ret, _Dir.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int Phase
    # @param int Mul
    # @param int SyncClr
    # @return int ret
    # @return int Phase
    # @return int Mul
    # @return int SyncClr
    def GetCntmOperationMode(self, Id, ChNo, Phase, Mul, SyncClr):
        """
        ### breif
        プロト関数: AioGetCntmOperationMode
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ Phase
        * _int_ Mul
        * _int_ SyncClr
        \n### return
        * _int_ ret
        * _int_ Phase
        * _int_ Mul
        * _int_ SyncClr
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _Phase = c_short(Phase)
        _Mul = c_short(Mul)
        _SyncClr = c_short(SyncClr)

        ret = AioGetCntmOperationMode(_Id, _ChNo, byref(_Phase), byref(_Mul), byref(_SyncClr))

        return ret, _Phase.value, _Mul.value, _SyncClr.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int FilterValue
    # @return int ret
    # @return int FilterValue
    def GetCntmDigitalFilter(self, Id, ChNo, FilterValue):
        """
        ### breif
        プロト関数: AioGetCntmDigitalFilter
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ FilterValue
        \n### return
        * _int_ ret
        * _int_ FilterValue
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _FilterValue = c_short(FilterValue)

        ret = AioGetCntmDigitalFilter(_Id, _ChNo, byref(_FilterValue))

        return ret, _FilterValue.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int PlsWidth
    # @return int ret
    # @return int PlsWidth
    def GetCntmPulseWidth(self, Id, ChNo, PlsWidth):
        """
        ### breif
        プロト関数: AioGetCntmPulseWidth
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ PlsWidth
        \n### return
        * _int_ ret
        * _int_ PlsWidth
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _PlsWidth = c_short(PlsWidth)

        ret = AioGetCntmPulseWidth(_Id, _ChNo, byref(_PlsWidth))

        return ret, _PlsWidth.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int ChNum
    # @return int ret
    # @return int ChNo
    def CntmStartCount(self, Id, ChNo, ChNum):
        """
        ### breif
        プロト関数: AioCntmStartCount
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ ChNum
        \n### return
        * _int_ ret
        * _int_ ChNo
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _ChNum = c_short(ChNum)

        ret = AioCntmStartCount(_Id, byref(_ChNo), _ChNum)

        return ret, _ChNo.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int ChNum
    # @return int ret
    # @return int ChNo
    def CntmStopCount(self, Id, ChNo, ChNum):
        """
        ### breif
        プロト関数: AioCntmStopCount
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ ChNum
        \n### return
        * _int_ ret
        * _int_ ChNo
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _ChNum = c_short(ChNum)

        ret = AioCntmStopCount(_Id, byref(_ChNo), _ChNum)

        return ret, _ChNo.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int ChNum
    # @return int ret
    # @return int ChNo
    def CntmPreset(self, Id, ChNo, ChNum):
        """
        ### breif
        プロト関数: AioCntmPreset
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ ChNum
        \n### return
        * _int_ ret
        * _int_ ChNo
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _ChNum = c_short(ChNum)

        ret = AioCntmPreset(_Id, byref(_ChNo), _ChNum)

        return ret, _ChNo.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int ChNum
    # @return int ret
    # @return int ChNo
    def CntmZeroClearCount(self, Id, ChNo, ChNum):
        """
        ### breif
        プロト関数: AioCntmZeroClearCount
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ ChNum
        \n### return
        * _int_ ret
        * _int_ ChNo
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _ChNum = c_short(ChNum)

        ret = AioCntmZeroClearCount(_Id, byref(_ChNo), _ChNum)

        return ret, _ChNo.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int ChNum
    # @return int ret
    # @return int ChNo
    def CntmReadCount(self, Id, ChNo, ChNum):
        """
        ### breif
        プロト関数: AioCntmReadCount
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ ChNum
        \n### return
        * _int_ ret
        * _int_ ChNo
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _ChNum = c_short(ChNum)

        ret = AioCntmReadCount(_Id, byref(_ChNo), _ChNum)

        return ret, _ChNo.value

    # @breif
    # @param int Id
    # @param int ChNo
    # @return int ret
    def CntmReadStatusEx(self, Id, ChNo):
        """
        ### breif
        プロト関数: AioCntmReadStatusEx
        ### args
        * _int_ Id
        * _int_ ChNo
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)

        ret = AioCntmReadStatusEx(_Id, _ChNo)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int RegNo
    # @param int Count
    # @return int ret
    def CntmNotifyCountUp(self, Id, ChNo, RegNo, Count):
        """
        ### breif
        プロト関数: AioCntmNotifyCountUp
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ RegNo
        * _int_ Count
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _RegNo = c_short(RegNo)
        _Count = c_ulong(Count)

        ret = AioCntmNotifyCountUp(_Id, _ChNo, _RegNo, _Count)

        return ret

    # @breif
    # @param int Id
    # @param int ChNo
    # @param int RegNo
    # @return int ret
    def CntmStopNotifyCountUp(self, Id, ChNo, RegNo):
        """
        ### breif
        プロト関数: AioCntmStopNotifyCountUp
        ### args
        * _int_ Id
        * _int_ ChNo
        * _int_ RegNo
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _ChNo = c_short(ChNo)
        _RegNo = c_short(RegNo)

        ret = AioCntmStopNotifyCountUp(_Id, _ChNo, _RegNo)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmCountUpCallbackProc(self, Id):
        """
        ### breif
        プロト関数: AioCntmCountUpCallbackProc
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmCountUpCallbackProc(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmNotifyCounterError(self, Id):
        """
        ### breif
        プロト関数: AioCntmNotifyCounterError
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmNotifyCounterError(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmStopNotifyCounterError(self, Id):
        """
        ### breif
        プロト関数: AioCntmStopNotifyCounterError
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmStopNotifyCounterError(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmCounterErrorCallbackProc(self, Id):
        """
        ### breif
        プロト関数: AioCntmCounterErrorCallbackProc
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmCounterErrorCallbackProc(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmNotifyCarryBorrow(self, Id):
        """
        ### breif
        プロト関数: AioCntmNotifyCarryBorrow
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmNotifyCarryBorrow(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmStopNotifyCarryBorrow(self, Id):
        """
        ### breif
        プロト関数: AioCntmStopNotifyCarryBorrow
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmStopNotifyCarryBorrow(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmCarryBorrowCallbackProc(self, Id):
        """
        ### breif
        プロト関数: AioCntmCarryBorrowCallbackProc
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmCarryBorrowCallbackProc(_Id)

        return ret

    # @breif
    # @param int Id
    # @param int TimeValue
    # @return int ret
    def CntmNotifyTimer(self, Id, TimeValue):
        """
        ### breif
        プロト関数: AioCntmNotifyTimer
        ### args
        * _int_ Id
        * _int_ TimeValue
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _TimeValue = c_ulong(TimeValue)

        ret = AioCntmNotifyTimer(_Id, _TimeValue)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmStopNotifyTimer(self, Id):
        """
        ### breif
        プロト関数: AioCntmStopNotifyTimer
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmStopNotifyTimer(_Id)

        return ret

    # @breif
    # @param int Id
    # @return int ret
    def CntmTimerCallbackProc(self, Id):
        """
        ### breif
        プロト関数: AioCntmTimerCallbackProc
        ### args
        * _int_ Id
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)

        ret = AioCntmTimerCallbackProc(_Id)

        return ret

    # @breif
    # @param int Id
    # @param int Reserved
    # @return int ret
    def CntmInputDIByte(self, Id, Reserved):
        """
        ### breif
        プロト関数: AioCntmInputDIByte
        ### args
        * _int_ Id
        * _int_ Reserved
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _Reserved = c_short(Reserved)

        ret = AioCntmInputDIByte(_Id, _Reserved)

        return ret

    # @breif
    # @param int Id
    # @param int AiomChNo
    # @param int Reserved
    # @return int ret
    def CntmOutputDOBit(self, Id, AiomChNo, Reserved):
        """
        ### breif
        プロト関数: AioCntmOutputDOBit
        ### args
        * _int_ Id
        * _int_ AiomChNo
        * _int_ Reserved
        \n### return
        * _int_ ret
        """
        _Id = c_short(Id)
        _AiomChNo = c_short(AiomChNo)
        _Reserved = c_short(Reserved)

        ret = AioCntmOutputDOBit(_Id, _AiomChNo, _Reserved)

        return ret
