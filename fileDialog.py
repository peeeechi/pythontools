# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 08:33:47 2017

@author: 1247065
"""

import tkinter
from tkinter import messagebox
from tkinter import filedialog
import os

def setDirctory(initialdir=os.path.curdir):
    root = tkinter.Tk()
    root.withdraw()    #←これでTkの小さいウィンドウが非表示になる。
    dirname=filedialog.askdirectory(initialdir=initialdir)

    return dirname

def readPath(fileType=[('*.*')],initialdir=os.path.curdir,multiFile = True):
    
    root = tkinter.Tk()
    root.withdraw()    #←これでTkの小さいウィンドウが非表示になる。
    showName =""
    ftypes = ""
    if len(fileType) == 0:
        showName = "全てのファイル"
        ftypes = "*.*"
    elif len(fileType) == 1:
        if fileType[0] == '*.*':
            showName = "全てのファイル"
            ftypes = "*.*"
        else:
            
            showName = fileType[0] + "ファイル"
            ftypes = "*." + fileType[0]
    else:
        for t in fileType:
            showName += t + "ファイル,"
            ftypes += "*." + t + ";"
        
    fTyp=[(showName,ftypes)]
    #askopenfilenames 複数ファイルを選択する。
    if multiFile == True:
        fileNames=filedialog.askopenfilenames(filetypes=fTyp,initialdir=initialdir)
    else:
        fileNames=filedialog.askopenfilename(filetypes=fTyp,initialdir=initialdir)
    return fileNames

def savePath(fileType=[('*.*')],initialdir=os.path.curdir):
    root = tkinter.Tk()
    root.withdraw()    #←これでTkの小さいウィンドウが非表示になる。
    showName =""
    ftypes = ""
    if len(fileType) == 0:
        showName = "全てのファイル"
        ftypes = "*.*"
    elif len(fileType) == 1:
        if fileType[0] == '*.*':
            showName = "全てのファイル"
            ftypes = "*.*"
        else:
            
            showName = fileType[0] + "ファイル"
            ftypes = "*." + fileType[0]
    else:
        for t in fileType:
            showName += t + "ファイル,"
            ftypes += "*." + t + ";"
        
    fTyp=[(showName,ftypes)]
    fileName = filedialog.asksaveasfilename(filetypes=fTyp,initialdir=initialdir)
    
    
    return fileName